<?php

namespace Modules\Inventory\Entities\Traits;

use Modules\Inventory\Entities\deceasedModel;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Event;

trait ModelEventLogger {

    /**
     * Automatically boot with Model, and register Events handler.
     */
    protected static function bootModelEventLogger()
    {
        foreach (static::getModelEvents() as $eventName) {
            static::$eventName(function (Model $model) use ($eventName) {
                try {
                    $reflect = new \ReflectionClass($model);
                    Event::fire('mdl.'.$eventName, array($model, static::getOldInfo())); 
                } catch (\Exception $e) {
                    return true;
                }
            });
        }
    }

    /**
     * Set the default events to be recorded if the $recordEvents
     * property does not exist on the model.
     *
     * @return array
     */
    protected static function getModelEvents()
    {
        if (isset(static::$recordEvents)) {
            return static::$recordEvents;
        }

        return [
            'created',
            'updated',
            'deleted',
        ];
    }

    // Get model type {deceased, payor, etc} for Event fire
    protected static function getOldInfo(){
        if (isset(static::$before)) {
            return static::$before;
        }
        else{
            return '';
        }
    }
} 

?>