<?php

namespace Modules\Inventory\Entities;

use Modules\Inventory\Entities\Traits\ModelEventLogger;
use Illuminate\Support\Facades\Event;
use Illuminate\Database\Eloquent\Model;

use Modules\Inventory\Entities\deceasedModel as DM;

class deceasedModel extends BaseModel
{
    protected $table = 'deceased_person_info';
    protected $fillable = ['f_name', 'l_name', 'm_name', 'ext_name', 'gender', 'age'];
    protected static $recordEvents, $before;

    protected $rules = array(
            'fname' => 'Required|min:3|Max:50|regex:/^[A-z][A-z\s\.\-\']+$/',
            'mname' => 'min:3|Max:50|regex:/^[A-z][A-z\s\.\-\']+$/',
            'lname' => 'Required|min:3|Max:50|regex:/^[A-z][A-z\s\.\-\']+$/',
            'ename' => 'min:0|Max:10',
            'rd-gender' => 'Required',
            'age' => 'Required|min:0|max:200',
            'city' => 'Required|min:3',
            'brgy' => 'Required|min:3',
    );

    public function setInfo($request)
    {
        $DM = new DM;
        $DM->f_name=$request->input('fname');
        $DM->m_name=$request->input('mname');
        $DM->l_name=$request->input('lname');
        $DM->ext_name=$request->input('ename');
        $DM->gender=$request->input('rd-gender');
        $DM->age=$request->input('age');
        $DM->save();
        
    }

    public function updateInfo($request)
    {
        static::$before = DM::find($request->input('d_id'));

        $dm = DM::find($request->input('d_id'));
        $dm->f_name=$request->input('fname');
        $dm->m_name=$request->input('mname');
        $dm->l_name=$request->input('lname');
        $dm->ext_name=$request->input('ename');
        $dm->gender=$request->input('rd-gender');
        $dm->age=$request->input('age');
            
        $dm->save();
    }
}
