<?php

namespace Modules\Inventory\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class FileAttachModel extends BaseModel
{
    protected $table = 'file_attachments';
    protected $fillable = ['tracking_','owner','attachment_name','exten','path','size'];
    protected $primaryKey = 'attachment_id';

    protected $rules = array(
        'tracking_' => 'Required',
        'owner'=>'Required',
        'attachment_name' => 'Required',
        'exten' => 'Required',
        'path'=> 'Required',
        'size'=> 'Required',
        
    );


}
