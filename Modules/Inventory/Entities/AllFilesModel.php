<?php

namespace Modules\Inventory\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class AllFilesModel extends BaseModel
{
    protected $table = 'bghmc_allfiles';
    protected $fillable = ['tracking_no','file_name','filetype_id', 'receiverID','deptID_of_receiver','senderID','deptID_of_sender','attachmentID','remarks','_accepted'];
    protected $primaryKey = 'tracking_no';

    protected $rules = array(
        'file_name' => 'Required|min:3',
        'filetype_id' => 'Required',
        'receiverID'=> 'Required',
        'deptID_of_receiver' => 'Required',
        'senderID'=> 'Required',
        'deptID_of_sender' => 'Required',
        '_accepted'=> 'Required'
    );


}
