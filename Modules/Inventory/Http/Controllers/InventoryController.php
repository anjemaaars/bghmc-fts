<?php

namespace Modules\Inventory\Http\Controllers;
use Modules\Setup\Init;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Auth;
use Modules\Inventory\Entities\FileMngmtModel;
use Illuminate\Support\Facades\Storage;
use Modules\Administrator\Entities\SystemLogsModel as SLM;


class InventoryController extends Controller
{
   /**
     * Display a listing of the resource.
     * @return Response
     */
    protected $data;
    protected $page_title = 'Login';

    function setup($vars = null)
    {
        $Init = new Init;
        $vars['page'] = $this->page_title;
        $this->data['template'] = $Init->setup($vars);
        $logs = new SLM;
        $this->data['logs'] = $logs->show_logs();
        return $this->data;
    }

    public function index()
    {
        $emp_info = DB::table('bghmc_employee_info')->WHERE('emp_id', Session::get('bghmcuser')->emp_id)->first();
        if(password_verify(1, $emp_info->password)){
            return redirect()->route('accnt.passchange');
        }else{
        // === user ===
            $userHere=Auth::user()->emp_id;
        // === file type ===
            $ftypes=DB::table('bghmc_filetypes')->get();
        // === departments ===
            $allDepartments = DB::table('bghmc_departments')->get();
        // ============= counter for important files =============
            $ImpCtr = db::table('bghmc_allfiles')->where('_important',1)->where('ownerID', $userHere)->count();

        // ============= counter for Received files =============
            $recCTR = db::table('bghmc_allfiles')->where('ownerID',$userHere)->where('receiverID',$userHere)->count();

        // ============= counter for UNREAD files ============= ---------- unread = 0; read = 1;
            $UNRCTR = db::table('bghmc_allfiles')->where('ownerID',$userHere)->where('receiverID',$userHere)->Where('_read','0')->count();













            // === FOR CREATING FILE === ------search for receiver
            $emps=DB::table('bghmc_employee_info')->where('emp_id','!=',$userHere)->join('bghmc_departments','bghmc_departments.dept_id','=','bghmc_employee_info.dept_id')->get();

            $allbghmc_departments = DB::table('bghmc_departments')->get();

            
                        
            

            $this->data['ftypes'] = $ftypes;
            $this->data['emps'] = $emps;   
            $this->data['loser'] = $userHere;
            $this->data['impCTR'] = $ImpCtr; $this->data['recCTR'] = $recCTR; $this->data['UNRCTR'] = $UNRCTR;
            








            // =====IMPORTANT UNREAD FILES=====
                // ===== READ FILE IS EQUAL TO  ___ IMPORTANT FILE IS EQUAL TO 1  =====
            $all_fileeess=DB::table('bghmc_allfiles')
            ->where('ownerID',$userHere)
            ->join('file_attachments','file_attachments.attachment_id','=','bghmc_allfiles.attachmentID')
            ->get();
            

            foreach($all_fileeess as $f){
                    // _______NAMESID to NAME NAME_______
                    $Snamesss = DB::table('bghmc_employee_info')->select('f_name','l_name','dept_id')->where('emp_id',$f->receiverID)->orderBy('created_at', 'desc')->first();
                        $f->_extra1 = $Snamesss->f_name . ' ' . $Snamesss->l_name;
                    $Rnamesss = DB::table('bghmc_employee_info')->select('f_name','l_name','dept_id')->where('emp_id',$f->senderID)->orderBy('created_at', 'desc')->first();
                        $f->_extra2 = $Rnamesss->f_name . ' ' . $Rnamesss->l_name;
                    // _______DEPTID TO DEPARTMENT NAMES_______
                    $deptOfUser = $Snamesss->dept_id;
                    $departs = DB::table('bghmc_departments')->select('dept_name')->where('dept_id',$deptOfUser)->first();
                        $f->deptID_of_receiver = $departs->dept_name;   
                    // _______DEPTID TO DEPARTMENT NAMES_______
                    $deptOfUser = $Rnamesss->dept_id;
                    $departs = DB::table('bghmc_departments')->select('dept_name')->where('dept_id',$deptOfUser)->first();
                        $f->deptID_of_receiver = $departs->dept_name;   
                 }
                $tite_page = 'Home';
                $this->data['per_files'] = $all_fileeess;          


            // return Datatables::of(DB::table('bghmc_allfiles')->query())->make(true);
            return view('inventory::files\_impFiles',$this->setup(),compact('tite_page'));
        }
    }



   
}
