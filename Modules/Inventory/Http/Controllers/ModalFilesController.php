<?php

namespace Modules\Inventory\Http\Controllers;
use Modules\Setup\Init;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;   
use Modules\Inventory\Entities\AllFilesModel;
use Modules\Inventory\Entities\FileAttachModel;
use Illuminate\Support\Facades\Storage;
use Modules\Administrator\Entities\SystemLogsModel as SLM;

class ModalFilesController extends Controller
{
   /**
     * Display a listing of the resource.
     * @return Response
     */
    protected $data;
    protected $page_title = 'Specific_File';

    function setup($vars = null)
    {
        $Init = new Init;
        $vars['page'] = $this->page_title;
        $this->data['template'] = $Init->setup($vars);
        $logs = new SLM;
        $this->data['logs'] = $logs->show_logs();
        return $this->data;

    }

    public function show($id)
    {
            $userHere=Auth::user()->emp_id;
            $ftypes=DB::table('bghmc_filetypes')->get();
            $allbghmc_departments = DB::table('bghmc_departments')->get();
            $emps=DB::table('bghmc_employee_info')->where('emp_id','!=',$userHere)->join('bghmc_departments','bghmc_departments.dept_id','=','bghmc_employee_info.dept_id')->get();
            $a = AllFilesModel::where('ownerID',$userHere)->where('tracking_no',$id)->first();
            $b = $a->ownerID;
            $c = $a->receiverID;
            $d = $a->senderID;
            if (($b) == $userHere){
                if(($b) == ($c)){
                    return 'Receiver';
                }elseif(($b)==($d)){
                    $per_files = AllFilesModel::where('ownerID',$userHere)->where('tracking_no',$id)
                    ->join('file_attachments','file_attachments.tracking_','=','bghmc_allfiles.attachmentID')
                    ->join('bghmc_departments','bghmc_departments.dept_id','=','bghmc_allfiles.deptID_of_receiver')
                    ->join('bghmc_employee_info','bghmc_employee_info.emp_id','=','bghmc_allfiles.receiverID')
                    ->join('bghmc_filetypes','bghmc_filetypes.filetype_id','=','bghmc_allfiles.filetype_id')
                    ->first();
                    
                }else{return redirect()->back()->with('message','SOMETHING WENT WRONG WITH THE SERVER! PLEASE TRY AGAIN LATER. (=^_^=)'); }
            }else{return redirect()->back()->with('message','SOMETHING WENT WRONG WITH THE SERVER! PLEASE TRY AGAIN LATER. (=^_^=)'); }

            $atta = FileAttachModel::where('owner',$userHere)->where('tracking_',$id)->first();
            $hell = $atta->attachment_name;
            

            $fileattach = Storage::get('FileAttachments/'.$userHere.'/'. $per_files->attachment_name);
            // return $fileattach;
            DB::table('bghmc_allfiles')->where('ownerID',$userHere)->where('tracking_no',$id)->update(['_read'=>'1']);    

            $tite_page = $per_files->file_name . ' for ' . $per_files->f_name;
            $this->data['tite_page']=$tite_page;
            $this->data['ftypes'] = $ftypes;
            $this->data['emps'] = $emps; 
            $this->data['fileattach'] = $fileattach; 
            $this->data['allbghmc_departments'] = $allbghmc_departments;
            $this->data['per_files']=$per_files;
            return view('inventory::files\_specific_file',$this->setup());  
    }
}  