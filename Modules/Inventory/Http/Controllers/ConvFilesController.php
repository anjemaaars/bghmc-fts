<?php

namespace Modules\Inventory\Http\Controllers;
use Modules\Setup\Init;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;   
use Modules\Inventory\Entities\AllFilesModel;
use Modules\Inventory\Entities\FileMngmtModel;
use Modules\Inventory\Entities\FileAttachModel;
use Modules\Inventory\Entities\FileArchiveModel;
use Modules\Inventory\Resources\views\files\upload;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
class ConvFilesController extends Controller

{
   /**
     * Display a listing of the resource.
     * @return Response
     */
    protected $data;
    protected $page_title = 'Files';

    function setup($vars = null){   
        $Init = new Init;
        $vars['page'] = $this->page_title;
        $this->data['template'] = $Init->setup($vars);
        return $this->data;}

    public function show($id){
        $owner = Auth::user()->emp_id;
        $file = AllFilesModel::where('tracking_no',$id)->first();
        $attch = FileAttachModel::where('tracking_',$id)->first();
        $attchName = $attch->attachment_name;
        
        // ====================== ARCHIVE THE FILE FIRST ===================
        FileArchiveModel::create([
        'tracking_no' => $file->tracking_no,
        'fileName' => $file->file_name,
        'fileType' => $file->filetype_id,
        'ownerID'=> $file->ownerID,
        'Rid'=> $file->receiverID,
        'RdeptID' => $file->deptID_of_receiver,
        'Sid'=> $file->senderID,
        'SdeptID' => $file->deptID_of_sender,
        'attachID'=> $file->attachmentID,
        'remarks'=>$file->remarks,
        '_imp'=>'0',
        '_extra1'=>'0',
        '_extra2'=>'0',
        ]);
        // =========== ATTACHMENT ARCHIVING ==========
        // Storage::put('FileAttachmentsArchived/'.$owner);
        // Storage::file()->move('FileAttachmentsArchived/'.$owner, $attchName);

        // ====================== DELETE THE FILE HERE ===================
        AllFilesModel::where('tracking_no',$id)->where('ownerID',$owner)->delete();
        FileAttachModel::where('tracking_',$id)->where('owner',$owner)->delete();
        Storage::delete('FileAttachments/'.$owner.'/'. $attchName);
        return redirect()->back()->with('message','Message Deleted!');
    }
    public function store(Request $request){return $this->send($request);}
    public function send($request){
        $UrgentFile = $request['Urgent'];
        $filetype = $request['filetype_id'];
        $owner = Auth::user()->emp_id;
        $ownerdept = DB::table('bghmc_employee_info')->select('dept_id')->where('emp_id',$owner)->first();
        $receivers = DB::table('bghmc_employee_info')->where('emp_id',$request['receiver'])->first();

// ========== CHECKS IF FILE IS ATTACHED ==========
        if (($request['fileToUpload']) == ''){
            // ========== CHECKS IF TABLE IS EMPTY OR NOT ===========        
            $tbCounter = DB::table('bghmc_allfiles')->select('tracking_no')->count();
            if(($tbCounter)==0){$idss = 100000;}
            else{$ids = DB::table('bghmc_allfiles')->select('tracking_no')->first(); $idss = $ids->tracking_no + '1'; $aID = 0;}
        }else{
            // =========== ATTACHMENT STORING ==========
            $fileAttachment = $request->file('fileToUpload');
            $fileUplaod = $request['fileToUpload'];
            $name = $fileAttachment->getClientOriginalName();
            $path = md5(time().$name).$name;
            Storage::putFileAs('FileAttachments/'.$owner, $fileUplaod,$name);
            Storage::putFileAs('FileAttachments/'.$receivers->emp_id, $fileUplaod,$name);    
            // ========== CHECKS IF TABLE IS EMPTY OR NOT ===========        
            $tbCounter = DB::table('bghmc_allfiles')->select('tracking_no')->count();
            if(($tbCounter)==0){$idss = 100000;}
            else{$ids = DB::table('bghmc_allfiles')->select('tracking_no')->first(); $idss = $ids->tracking_no + '1'; $aID = $idss;}
            $target_dir = "uploads/";
            $fileAttachment->move($target_dir, $fileUplaod->getClientOriginalName());
        }
        
        
        
// ========== FILE COPY OF THE SENDER ===========
        AllFilesModel::create([
        'tracking_no' => $idss,
        'file_name' => $request['file_name'],
        'filetype_id' => $request['filetype_id'],
        'ownerID'=> $owner,
        'receiverID'=> $request['receiver'],
        'deptID_of_receiver' => $receivers->dept_id,
        'senderID'=> $owner,
        'deptID_of_sender' => $ownerdept->dept_id,
        'attachmentID'=> $aID,
        'remarks'=>$request['fileText'],
        '_important'=>$UrgentFile,
        '_read'=>'1',
        '_extra1'=>'0',
        '_extra2'=>'0',
        '_accepted' => '0',]);
        FileAttachModel::create([
        'tracking_' => $idss,
        'owner'=> $owner,
        'attachment_name' => $fileUplaod->getClientOriginalName(),
        'exten' => $fileUplaod->getClientOriginalExtension(),
        'path' => $fileUplaod->getRealPath(),
        'size' => $fileUplaod->getSize(),]);        
// ========== FILE COPY OF THE RECEIVER ===========
        AllFilesModel::create([
        'tracking_no' => $idss, 'file_name' => $request['file_name'],
        'filetype_id' => $request['filetype_id'],
        'ownerID'=> $request['receiver'],
        'receiverID'=> $owner, 'deptID_of_receiver' => $receivers->dept_id,
        'senderID'=> $owner, 'deptID_of_sender' => $ownerdept->dept_id,
        'attachmentID'=> $idss,
        'remarks'=>$request['fileText'],
        '_important'=>$UrgentFile,
        '_read'=> '0',
        '_extra1'=>'0','_extra2'=>'0','_accepted' => '0',]);
        FileAttachModel::create([
        'tracking_' => $idss,
        'owner'=> $request['receiver'],
        'attachment_name' => $fileUplaod->getClientOriginalName(),
        'exten' => $fileUplaod->getClientOriginalExtension(),
        'path' => $fileUplaod->getRealPath(),
        'size' => $fileUplaod->getSize(),]);
    
        return redirect()->back()->with('message','Message Sent!');
    }

    public function destroy(AllFilesModel $a, $b){
        return 'asdasdasd';
    }



    public function download($upload_id, $filename)
    {
        $filename = base64_decode($filename);
     
        $this->checkForValidToken($filename);
     
        $upload = Context::get('fileToUpload')->model();
     
        $file = $this->getFileFromStorage($upload->path, $filename);
     
        Storage::disk('pathal')->put($upload->path, $file);
     
        $path = storage_path(sprintf('app/%s', $upload->path));
     
        return response()->download($path, $upload->name);
    }

   
}
