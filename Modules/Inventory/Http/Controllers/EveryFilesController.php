<?php
namespace Modules\Inventory\Http\Controllers;
use Modules\Setup\Init;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;   
use Modules\Inventory\Entities\AllFilesModel;
use Modules\Inventory\Entities\FileMngmtModel;
use Modules\Inventory\Entities\FileAttachModel;
use Modules\Inventory\Resources\views\files\upload;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Modules\Administrator\Entities\SystemLogsModel as SLM;

class EveryFilesController extends Controller
{
   /**
     * Display a listing of the resource.
     * @return Response
     */
    protected $data;
    protected $page_title = 'Files';

    function setup($vars = null)
    {
        $Init = new Init;
        $vars['page'] = $this->page_title;
        $this->data['template'] = $Init->setup($vars);
        $logs = new SLM;
        $this->data['logs'] = $logs->show_logs();
        return $this->data;

    }

    public function show($id){   
        $userHere=Auth::user()->emp_id;



// ______MARK FILES IMPORTANT_______ 1 = important; 0 = not important
            $fileMUNA = DB::table('bghmc_allfiles')
            ->select('_important')
            ->where('tracking_no',$id)
            ->where('ownerID',$userHere)->first();

            if( ($fileMUNA->_important)== 0 ){
                $files_na_important = DB::table('bghmc_allfiles')
                ->where('tracking_no',$id)
                ->where('ownerID',$userHere)
                ->update(['_important'=>'1',]);    
                return redirect()->back()->with('message','MARKED AS IMPORTANT!'); 
            }elseif( ($fileMUNA->_important)== 1 ){
                $file_na_important = DB::table('bghmc_allfiles')
                ->where('tracking_no',$id)
                ->where('ownerID',$userHere)
                ->update(['_important'=>'0',]);    
                return redirect()->back()->with('message','MARKED AS NOT IMPORTANT!'); 
            }else{
                return redirect()->back()->with('message','SOMETHING WENT WRONG WITH THE SERVER! PLEASE TRY AGAIN LATER. (=^_^=)'); 
            }
            
            
        if($id='ERROR'){
            return redirect()->back()->with('message','SOMETHING WENT WRONG WITH THE SERVER! PLEASE TRY AGAIN LATER. THANK YOU (=^_^=)'); 
        }


    }
    public function allFiles(){
        $userHere=Auth::user()->emp_id;
        // ===== ALL FILES BASED ON EMPLOYEE ID =====
        $all_fileees=DB::table('bghmc_allfiles')
        ->join('file_attachments','file_attachments.tracking_','=','bghmc_allfiles.attachmentID')
        ->where('owner',$userHere)
        ->where('ownerID',$userHere)
        ->get();

        // _________ALL FILES________
        foreach($all_fileees as $f){
            // _______NAMESID to NAME NAME_______
            $Snamesss = DB::table('bghmc_employee_info')->select('f_name','l_name','dept_id')->where('emp_id',$f->receiverID)->orderBy('created_at', 'desc')->first();
                $f->_extra1 = $Snamesss->f_name . ' ' . $Snamesss->l_name;
            $Rnamesss = DB::table('bghmc_employee_info')->select('f_name','l_name','dept_id')->where('emp_id',$f->senderID)->orderBy('created_at', 'desc')->first();
                $f->_extra2 = $Rnamesss->f_name . ' ' . $Rnamesss->l_name;
            // _______DEPTID TO DEPARTMENT NAMES_______
            $deptOfUser = $Snamesss->dept_id;
            $departs = DB::table('bghmc_departments')->select('dept_name')->where('dept_id',$deptOfUser)->first();
                $f->deptID_of_receiver = $departs->dept_name;   
            $deptOfUserSender = $Rnamesss->dept_id;
            $Sdeparts = DB::table('bghmc_departments')->select('dept_name')->where('dept_id',$deptOfUserSender)->first();
                $f->deptID_of_sender = $Sdeparts->dept_name;   
        }
        $tite_page = 'All Files';
        $a = $this->data['per_files'] = $all_fileees;          
        return $this->returnsEverything($a,$tite_page,$userHere);

    } 
    public function importantFiles(){
        $userHere=Auth::user()->emp_id;
        // ===== IMPORTANT FILES =====
        $imp_files=DB::table('bghmc_allfiles')
        ->join('file_attachments','file_attachments.tracking_','=','bghmc_allfiles.attachmentID')
        ->Where('_important',1)
        ->where('ownerID',$userHere)
        ->where('owner',$userHere)->get();
        

        foreach($imp_files as $g){
            // _______NAMESID to NAME NAME_______
            $Snamesss = DB::table('bghmc_employee_info')->select('f_name','l_name','dept_id')->where('emp_id',$g->receiverID)->orderBy('created_at', 'desc')->first();
                $g->_extra1 = $Snamesss->f_name . ' ' . $Snamesss->l_name;
            $Rnamesss = DB::table('bghmc_employee_info')->select('f_name','l_name','dept_id')->where('emp_id',$g->senderID)->orderBy('created_at', 'desc')->first();
                $g->_extra2 = $Rnamesss->f_name . ' ' . $Rnamesss->l_name;
            // _______DEPTID TO DEPARTMENT NAMES_______
            $deptOfUser = $Snamesss->dept_id;
            $departs = DB::table('bghmc_departments')->select('dept_name')->where('dept_id',$deptOfUser)->first();
                $g->deptID_of_receiver = $departs->dept_name;   
            // _______DEPTID TO DEPARTMENT NAMES_______
            $deptOfUser = $Rnamesss->dept_id;
            $departs = DB::table('bghmc_departments')->select('dept_name')->where('dept_id',$deptOfUser)->first();
                $g->deptID_of_receiver = $departs->dept_name;   
         }

        $tite_page = 'Important Files';
        $a = $this->data['per_files'] = $imp_files;
        return $this->returnsEverything($a,$tite_page,$userHere);
    }
    public function receivedFiles(){
        $userHere=Auth::user()->emp_id;
        // ===== ALL RECEIVED FILES =====
        $received_fileees = DB::table('bghmc_allfiles')
        ->join('bghmc_departments','bghmc_departments.dept_id','=','bghmc_allfiles.deptID_of_receiver')
        ->join('bghmc_employee_info','bghmc_employee_info.emp_id','=','bghmc_allfiles.receiverID')
        ->join('file_attachments','file_attachments.tracking_','=','bghmc_allfiles.attachmentID')
        ->where('receiverID',$userHere)
        ->where('ownerID','=',$userHere)
        ->where('owner',$userHere)
        ->get();

        $tite_page = 'Received Files';
        $a = $this->data['per_files'] = $received_fileees;
        return $this->returnsEverything($a,$tite_page,$userHere);
    }
    public function sentFiles(){
        $userHere=Auth::user()->emp_id;
        // ===== ALL SENT FILES =====
        $sent_fileees=DB::table('bghmc_allfiles')
        ->join('bghmc_departments','bghmc_departments.dept_id','=','bghmc_allfiles.deptID_of_receiver')
        ->join('bghmc_employee_info','bghmc_employee_info.emp_id','=','bghmc_allfiles.receiverID')
        ->join('file_attachments','file_attachments.tracking_','=','bghmc_allfiles.attachmentID')
        ->where('senderID','=',$userHere)
        ->where('ownerID','=',$userHere)
        ->where('owner',$userHere)
        ->get();

        $tite_page = 'Sent Files';
        $a = $this->data['per_files'] = $sent_fileees;
        return $this->returnsEverything($a,$tite_page,$userHere);
    }

    public function returnsEverything($a,$tite_page,$userHere){
        $ftypes=DB::table('bghmc_filetypes')->get();
        $emps=DB::table('bghmc_employee_info')
        ->where('emp_id','!=',$userHere)
        ->join('bghmc_departments','bghmc_departments.dept_id','=','bghmc_employee_info.dept_id')->get();
        
        $allbghmc_departments = DB::table('bghmc_departments')->get();

        $this->data['ftypes'] = $ftypes;
        $this->data['emps'] = $emps;           
        $this->data['allbghmc_departments'] = $allbghmc_departments;
        
        return view('inventory::files\_impfiles',$this->setup(),compact('tite_page'));
    }
}
