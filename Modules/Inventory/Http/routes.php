<?php

Route::group(['middleware' => ['web','auth'], 'prefix' => 'inventory', 'namespace' => 'Modules\Inventory\Http\Controllers'], function()
{
    // show page or next page
    Route::get('/', 'InventoryController@index')->name("inventory.index");
    Route::resource('EveryFiles','EveryFilesController');
    Route::resource('SpecificFile','ModalFilesController');
    Route::resource('Conversations','ConvFilesController');

    // ========== haha boom! ============
    Route::get('/inventory/INBOX','EveryFilesController@receivedFiles')->name('display.RECEIVED');
    Route::get('/inventory/OUTBOX','EveryFilesController@sentFiles')->name('display.SENT');
    Route::get('/inventory/MESSAGES','EveryFilesController@allFiles')->name('display.ALL');
    Route::get('/inventory/IMPORTANT','EveryFilesController@importantFiles')->name('display.IMPS');

    // ========== haha boom! ============
    Route::get('/admin/INBOX','EveryFilesController@receivedFiles')->name('displayAdmin.RECEIVED');
    Route::get('/admin/OUTBOX','EveryFilesController@sentFiles')->name('displayAdmin.SENT');
    Route::get('/admin/MESSAGES','EveryFilesController@allFiles')->name('displayAdmin.ALL');
    Route::get('/admin/IMPORTANT','EveryFilesController@importantFiles')->name('displayAdmin.IMPS');
});