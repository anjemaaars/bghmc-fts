 @extends('template::admin-pages.menus.'.$template['menu']) 

@section('plugins-css')
    <link rel="stylesheet" href="{{asset('adminlte')}}/plugins/datatables/dataTables.bootstrap.css">
@stop

@section('pagename')
    
    <span style="font-size: 25px;">
        @include('inventory::includes._titleFILE')
    </span>
@stop

@section('content')
    <div class="content-wrapper">
        <section class="content">
            @if (($tite_page)=='Home')
                @include('inventory::includes._homeShortcuts')
            @endif
            <div class="box">
                <div class="box-header">
                    
                </div>
                <div class="box-body">
                    <div class="col-lg-8">
                    <table class="table table-striped table-bordered" id="tablet">
                        <thead>
                            <tr>
                                <td></td>
                                <td>Tracking No.</td>
                                <td>File Name</td>
                                @if(($tite_page)=='Received Files')
                                    <td>Sender</td>
                                @elseif(($tite_page)=='Sent Files')
                                    <td>Receiver</td>
                                @else
                                    <td>Sender</td>
                                    <td>Receiver</td>
                                @endif
                                <td>Attachement</td>
                                <td></td>
                            </tr>
                            
                        </thead>
                        <tbody>
                            @foreach($per_files as $per_files)
                                <a href="{{ URL::route('SpecificFile.show',$per_files->tracking_no) }}">
                                <tr>
                                    <td>@include('inventory::includes._markImpFile')</td>
                                    <td>{{$per_files->tracking_no}}</td>
                                    <td>{{$per_files->file_name}}</td>
                                    
                                    @if(($tite_page)=='Received Files')
                                        <td>
                                            @if(($per_files->senderID) == (Auth::user()->emp_id))
                                                ME (=^_^=)!!!
                                            @else
                                                {{$per_files->dept_name}} <br>
                                                ---- {{$per_files->f_name}} {{$per_files->l_name}} ----
                                            @endif
                                        </td>
                                    @elseif(($tite_page)=='Sent Files')
                                        <td>
                                             @if(($per_files->receiverID) == (Auth::user()->emp_id))
                                                ME (=^_^=)!!!
                                            @else
                                                {{$per_files->dept_name}} <br>   
                                                ---- {{$per_files->f_name}} {{$per_files->l_name}} ----
                                            @endif
                                        </td>
                                        
                                    @else
                                        <td>
                                            @if(($per_files->senderID) == (Auth::user()->emp_id))
                                                ME (=^_^=)!!!
                                            @else
                                                {{$per_files->deptID_of_sender}} <br> 
                                                <a href="" data-toggle="modal" data-target="#modal-view-personnel"> 
                                                    ---- {{$per_files->_extra2}} ---- 
                                                </a>
                                            @endif
                                        </td>
                                        <td>
                                            @if(($per_files->receiverID) == (Auth::user()->emp_id))
                                                ME (=^_^=)!!!
                                            @else
                                                {{$per_files->deptID_of_receiver}} <br> 
                                                <a href="" data-toggle="modal" data-target="#modal-view-personnel">
                                                    ---- {{$per_files->_extra1}} ----
                                                </a>
                                            @endif
                                        </td>
                                    @endif
                                    <td>
                                        <a href="">{{$per_files->attachment_name}}</a>
                                    </td>
                                    <td>
                                        <a href="{{URL::route('SpecificFile.show',$per_files->tracking_no)}}">View</a>
                                        
                                    </td>
                                </tr>
                                </a>
                            @endforeach
                        </tbody>
                    </table>
                    @if(Session::has('message'))
                        <div class="alert alert-success pull-right alertSession" onload="alerts()">{{Session::get('message')}}</div>
                    @endif

                        
                    <!-- ___________ ALL INCLUDES _____________ -->
                    @include('inventory::includes._ALL')  
                    </div>
                    <div class="col-lg">
                        <button type="button" class="btn btn-new pull-right" data-toggle="modal" data-target="#modal-create-file"><i class="glyphicon glyphicon-pencil"></i> &nbsp; Compose</button> 
                        
                    </div>
                </div>
                


            </div>
        </section>
    </div>
@stop

@section('plugins-script')
    <script src="{{asset('adminlte')}}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{{asset('adminlte')}}/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script src="{{asset('adminlte')}}/plugins/daterangepicker/moment.js"></script>
    <script src="{{asset('js')}}/jquery.min.js"></script>
    <script src="{{asset('js')}}/moment.min.js"></script>
    <script src="{{asset('js')}}/fullcalendar.min.js"></script>

@stop