 @extends('template::admin-pages.menus.'.$template['menu']) 

@section('plugins-css')
    <link rel="stylesheet" href="{{asset('adminlte')}}/plugins/datatables/dataTables.bootstrap.css">
@stop

@section('pagename')
    <span style="font-size: 25px;">{{$tite_page}}</span>
@stop

@section('content')
    <div class="content-wrapper">
        <section class="content">
            <div class="box">
                <div class="box-header">
                    <button type="button" class="btn btn-new btn-lg pull-right" data-toggle="modal" data-target="#modal-create-file">Create File &nbsp;&nbsp;&nbsp;
                    <i class="fa fa-user-plus"></i> </button>
                </div>
                <div class="box-body">
                    <div id="searchFile">
                        <form>
                            <input type="text" name="searchFile" placeholder="Search...">
                        </form>
                    </div>
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <td></td>
                                <td>Tracking No.</td>
                                <td>File Name</td>
                                <td>Sender</td>
                                <td>Receiver</td>
                                <td>Department</td>
                                <td>Attachement</td>
                                <td>More</td>
                            </tr>
                            @foreach($per_files as $per_files)
                                <tr>
                                
                                    <td>
                                    @if(($per_files->_important)==0)
                                    <a href="{{ URL::route('EveryFiles.show',$per_files->tracking_no) }}" class="btn btn-function">
                                        <i class="glyphicon glyphicon-flag tooltip" title="Mark as Important" >
                                            <span class="tooltiptext">Mark as Important</span>    
                                        </i>
                                    </a>
                                    @elseif(($per_files->_important)==1)
                                    <a href="{{ URL::route('EveryFiles.show',$per_files->tracking_no) }}" class="btn btn-function">
                                        <i class="glyphicon glyphicon-heart tooltip" title="Mark as Not Important">
                                            <span class="tooltiptext">Mark as Not Important</span>
                                        </i>
                                    </a>
                                    @endif
                                    </td>
                                    <td>{{$per_files->tracking_no}}</td>
                                    <td>{{$per_files->file_name}}</td>
                                        <td>
                                            @if(($per_files->senderID) == (Auth::user()->emp_id))
                                                ME (=^_^=)!!!
                                            @else
                                                {{$per_files->_extra2}}
                                            @endif
                                        </td>
                                        <td>
                                             @if(($per_files->receiverID) == (Auth::user()->emp_id))
                                                ME (=^_^=)!!!
                                            @else
                                                {{$per_files->_extra1}}
                                            @endif
                                        </td>

                                    <td>{{$per_files->deptID_of_receiver}}</td>
                                    <td>{{$per_files->attachmentID}}</td>
                                    <td class="hidden-xs" >
                                        <a href="{{ URL::route('SpecificFile.show',$per_files->tracking_no) }}" class="btn btn-function" ><i class="glyphicon glyphicon-info-sign"></i></a>
                                        

                                    </td>
                                </tr>
                                
                            @endforeach
                        </thead>
                        <tbody>

                        </tbody>

                    </table>
                    @if(Session::has('message'))
                        <div class="alert alert-success pull-right alertSession" onload="alerts()">{{Session::get('message')}}</div>
                    @endif

                    <!-- _______CREATE FILE_______ -->
                    @include('inventory::files._createFile')

                    <!-- ____________EDIT FILE____________ -->
                    @include('inventory::files._editFile')


                      

                </div>
            </div>
        </section>
    </div>
@stop

@section('plugins-script')
    <script src="{{asset('adminlte')}}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{{asset('adminlte')}}/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script src="{{asset('adminlte')}}/plugins/daterangepicker/moment.js"></script>
    <script src="{{asset('js')}}/jquery.min.js"></script>
    <script src="{{asset('js')}}/moment.min.js"></script>
    <script src="{{asset('js')}}/fullcalendar.min.js"></script>

@stop