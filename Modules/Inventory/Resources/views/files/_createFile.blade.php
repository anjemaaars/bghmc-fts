<!-- ___________________MODAL TO CREATE NEW FILE__________________ -->
  <div class="modal fade" id="modal-create-file" type="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-body">
            <div class="msgTabs">
              <button>New Message</button>
              <!-- <button class="msgTabslinks" onclick="openMsg(event, 'UMsg')">Urgent Message</button> -->
              <button type="button" data-dismiss="modal" class="pull-right">Cancel</button>
            </div>
<!-- ==================================== CREATE NORMAL FILE ==================================== -->
            <!-- <div id="NMsg" class="msgTabscontent"> -->
            <div>
                @include('inventory::includes._createNFile')
            </div>
<!-- ==================================== CREATE URGENT FILE ==================================== -->
            <div id="UMsg" class="msgTabscontent">
                @include('inventory::includes._createUFile')
            </div>
        </div>
      </div>
    </div>
  </div>