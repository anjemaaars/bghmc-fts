 @extends('template::admin-pages.menus.'.$template['menu']) 

@section('plugins-css')
    <link rel="stylesheet" href="{{asset('adminlte')}}/plugins/datatables/dataTables.bootstrap.css">
@stop

@section('pagename')
    <span style="font-size: 25px;">{{$tite_page}}</span>
@stop

@section('content')
    <div class="content-wrapper">
      <section class="content">
        <div class="box">
          <div class="box-header">

              <a class="btn btn-default btn-lg nextStep" href="{{URL::route('EveryFiles.show','SENT_FILES')}}" data-toggle="modal" data-dismiss="modal"><span class="glyphicon glyphicon-arrow-left"></span>&nbsp; Back</a>
              
              <span style="border-bottom:1px solid black;">@include('inventory::includes._markImpFile')</span>
              <script type="text/javascript">
                var timestamp = 0;

                setInterval(function(){
                    $.ajax({
                        url:"confess_show.php?t=" + timestamp,
                        type:"GET",
                        dataType:"html",
                        success:function(data){
                            if(data != ''){ //Only append data if there are some new
                                timestamp = Math.round((new Date()).getTime() / 1000); //Set timestamp to current time
                                $('.content').append(data);
                            }
                        }
                    });
                }, 6000);
              </script>
          </div>
          <div class="box-body">
            <div class="col-md-9" style="margin-left: auto; margin-right: auto; border-right: 1px solid #ccc;">
              <h3>{{$per_files->file_name}}</h3>
                  
              @if(($per_files->senderID)==(Auth::user()->emp_id))
                
                <h4>To: <b> {{$per_files->f_name}} {{$per_files->l_name}}</b> of {{$per_files->dept_name}}</h4>
                <h5>DATE SENT: {{$per_files->created_at}}</h5>
              
              @else
              
                <h4>From: <b> {{$per_files->f_name}} {{$per_files->l_name}}</b> of {{$per_files->dept_name}}</h4>
                <h5>DATE Received:     {{$per_files->created_at}}</h5>
              
              @endif

              <hr style="width:50%;">
              {{$per_files->remarks}}
              <br>
              <a href="">{{$per_files->attachment_name}}</a>
              
              <br><hr>
            </div>
            <!-- ============ BUTTON STUFF ===================== -->
            <div class="col-md-3 moreMenu" style="padding:0px;">
              <ul style="list-style-type: none; padding: 0px;">
                <li style="padding-bottom: 10px;">
                  <a class="btn btn-new btn-block nextStep" href="#reply_modal" data-toggle="modal" data-dismiss="modal"><span class="glyphicon glyphicon-arrow-right"></span> &nbsp;Reply</a>
                </li>
                <li style="padding-bottom: 10px;">  
                  <a class="btn btn-new btn-block nextStep" href="#edit_file" data-toggle="modal" data-dismiss="modal"><span class="glyphicon glyphicon-share-alt"></span> &nbsp; Forward</a>
                </li>
                <li>
                  <a class="btn btn-new btn-block nextStep" href="#alert_modal" data-toggle="modal" data-dismiss="modal"><span class="glyphicon glyphicon-trash"></span> &nbsp; Delete</a>
                </li>
              </ul> 
            </div>

            <!-- ============================ MODAL FOR ALERT MESSAGE FOR DELETING MESSAGES =================== -->
            <div class="modal fade" id="alert_modal" type="dialog">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header" style="text-align: center;">
                    <h4>ARE YOU SURE?</h4>
                  </div>
                  <div class="modal-body" style="align-content: center; text-align: center;">
                    <form method="Delete" action="{{URL::route('Conversations.destroy',$per_files->tracking_no)}}">
                    {{ csrf_field() }}
                      <button type="submit" class="btn btn-primary btn-lg">Delete</button>
                      <button class="btn btn-new btn-lg" data-toggle="modal" data-dismiss="modal">Cancel</button>
                    </form>
                    </div>
                </div>
              </div>
            </div>


            <!-- =================================== MODAL FOR REPLYING A MESSAGE =============================== -->
            <div class="modal fade" id="reply_modal" type="dialog">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <h4>To: {{$per_files->f_name}} {{$per_files->l_name}}</h4>
                  </div>
                  <div class="modal-body">
                    <form method="Delete" action="{{URL::route('Conversations.index',$per_files->tracking_no)}}">
                    {{ csrf_field() }}
                      <!-- ____CONTENT____ -->
                      <div class="form-group">
                          <label class="control-label float-left" for="fileText">Message</label>
                          <div class="form-group form-group-pad">
                              <textarea type="text" class="form-control form-control-pad textContent" name="fileText" placeholder="Your Message"></textarea>
                          </div>
                      </div>
              <!-- _____URGENT FILE_____ -->
                      <div class="form-group">
                          <label class="control-label float-left" for="Urgent">Urgent? </label>
                          <input type="checkbox" name="Urgent">
                      </div>
                      
                      
                      <input type="text" name="Urgent" value="0" id="hideThis">
              <!-- ____ATTACHMENT____ -->
                      <div class="form-group">
                          <label class="control-label float-left" for="attach">Attach File</label>
                          <input type="file" name="fileToUpload" id="fileToUpload" class="btn btn-new btn-sm">
                      </div>

                      <div class='modal-footer'>
                        <button class="btn btn-new btn-lg" data-toggle="modal" data-dismiss="modal">Cancel</button>
                      </div>
                    </form>
                    </div>
                </div>
              </div>
            </div>



            <!-- ========================= MODAL FOR EDITING THE FORWARD MESSAGE =============================== -->
            @include('inventory::includes._editFile')









          </div>
          <div class="box-footer">
            @if(Session::has('message'))
                <div class="alert alert-success pull-right alertSession" onload="alerts()">{{Session::get('message')}}</div>
                
            @endif


          </div>
        </div>
      </section>
    </div>
@stop

@section('plugins-script')
    <script src="{{asset('adminlte')}}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{{asset('adminlte')}}/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script src="{{asset('adminlte')}}/plugins/daterangepicker/moment.js"></script>
    <script src="{{asset('js')}}/jquery.min.js"></script>
    <script src="{{asset('js')}}/moment.min.js"></script>
    <script src="{{asset('js')}}/fullcalendar.min.js"></script>

@stop

