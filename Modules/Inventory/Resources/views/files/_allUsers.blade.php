<!-- ___________________MODAL TO VIEW ALL USERS FOR SETTING RECEIVER __________________ -->
  <div class="modal fade" id="modal-allUsers-file" type="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Select Recepients</h4>
        </div>
        <div class="modal-body">
        	
        	<input type="text" id="searchNamesHere" class="form-control form-control-pad" onkeyup="dispTableFunction()" placeholder="Search Names..">
        	<input type="text" id="searchNamesHeres" class="form-control form-control-pad" onkeyup="dispTableFunctions()" placeholder="Search Departments..">

			<table class="table table-striped table-bordered table-hover" id="dispTable">
				<thead>
			  		<tr>
				    	<td>Name</td>
				    	<td>Department</td>
				 	</tr>
				</thead>
				<tbody>
					@foreach($emps as $emps)
						<tr onclick="selectedFunction();">
							<td>
								<input name="Recepients" type="checkbox" value="{{$emps->emp_id}}" id="checkboxes"> &nbsp; &nbsp; &nbsp;
								{{$emps->f_name}} {{$emps->l_name}}
							</td>
							<td>
								{{$emps->dept_name}}
							</td>
			  			</tr>
			  		@endforeach
			  	</tbody>
			</table>
        </div>
        <div class="modal-footer">
        	<a class="btn btn-new btn-lg previousStep" href="#modal-create-file" data-toggle="modal" data-dismiss="modal">Select</a>
        	<a class="btn btn-new btn-lg previousStep" href="#modal-create-file" data-toggle="modal" data-dismiss="modal">Cancel</a>
        </div>
        
      </div>
      
    </div>
  </div>



