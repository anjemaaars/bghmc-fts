<!-- ___________________MODAL TO CREATE NEW FILE__________________ -->
                      <div class="modal fade" id="modal-edit-file" role="dialog">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                              <h4 class="modal-title">New File</h4>
                            </div>
                            <div class="modal-body">
                                <form class="thing-form" role="form" name="newFile" id="newFile" method="post" >
                                    {{ csrf_field() }}


                                    <div class="form-group has-feedback">
                                        <div class="form-group">
                                            <label class="control-label float-left" for="fileName">File Name</label>
                                            <div class="form-group form-group-pad">
                                                <input type="text" class="form-control form-control-pad" name="fileName" placeholder="File Name">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label float-left" for="receiver">Receiver</label>
                                            <div class="form-group form-group-pad seee">
                                                <input type="text" class="form-control form-control-pad " name="receiver" placeholder="Employee Name or ID">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label float-left" for="fileText">Content</label>
                                            <div class="form-group form-group-pad">
                                                <textarea type="text" class="form-control form-control-pad textContent" name="fileText" placeholder="Your Message"></textarea>
                                            </div>

                                        </div>
                                    </div>
                                    <br>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-lg " data-dismiss="modal">Cancel</button>
                                        <button type="button" class="btn btn-new btn-lg" data-dismiss="modal">Save</button>
                                        <button type="button" class="btn btn-new btn-lg" data-dismiss="modal">Send File</button>
                                    </div>
                                </form>
                            </div>
                            
                          </div>
                          
                        </div>
                      </div>