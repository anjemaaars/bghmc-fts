<!-- @extends('template::admin-pages.menus.'.$template['menu']) -->
@extends('template::admin-pages.menus.administrators')

@section('plugins-css')
    <link rel="stylesheet" href="{{asset('adminlte')}}/plugins/datatables/dataTables.bootstrap.css">
@stop

@section('content')
  <div class="content-wrapper">
    <section class="content">
        <div class="pull-left" style="padding: 10px;">
            <a href="{{ route($backroute, null)}}" style="color: #342897; font-size: 20px;"><i class="glyphicon glyphicon-chevron-left"></i></a>
        </div>
        <div class="pull-left" style="font-size: 30px; padding-left: 10px; margin-bottom: 5px;">
            <span style="color: #000; font-size: 20px;">ID #{{$show_info->id}}</span>
        </div>
        <div style="margin-top: 60px;"></div>

        <div class="row">
            
            <div class="col-sm-4">
                <!--///////// DECEASED-->
                    <div class="box box-primary">
                        <div class="body-box">
                            <div class="container">
                                <div style="padding: 10px;">
                                    <b class="text-bts">DECEASED INFORMATION</b><button type="button" class="btn btn-function btn-edit" data-toggle="modal" data-target="#mdl-edit-deceased"><i class="glyphicon glyphicon-pencil"></i></button>
                                </div>
                                <table class="table">
                                    <tr>
                                        <td style="width: 150px;"><label>Name</label></td>
                                        <td><label>{{$show_info->l_name}}, {{$show_info->f_name}} {{$show_info->m_name}} @if($show_info->ext_name != null || $show_info->ext_name != "") ( {{$show_info->ext_name}} ) @endif</label></td>
                                    </tr>
                                    <tr>
                                        <td><label>Gender</label></td>
                                        <td>
                                            @if ($show_info->gender == 'f')
                                                Female
                                            @elseif ($show_info->gender == 'm')
                                                Male
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><label>Age</label></td>
                                        <td>{{$show_info->age}}</td>
                                    </tr>
                                    <tr>
                                        <td><label>Address</label></td>
                                        <td>
                                            {{$d_address->province}} , {{$d_address->city_mun}}<br>
                                            {{$d_address->brgy}} {{$d_address->details}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><label>Date added:</label></td>
                                        <td>{{$show_info->created_at}}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                                <!--<div class="deceased-img-col">-->
                                        <!--<img src="https://x1.xingassets.com/assets/frontend_minified/img/users/nobody_m.original.jpg" class="deceased-img">
                                        <br>-->
                                <!--</div>-->
                
                
            </div>
            <div class="col-sm-8">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#cremstatus" data-toggle="tab">Cremation Status and Details</a></li>
                        <li><a href="#payorinfo" data-toggle="tab">Payor Information</a></li>
                        @if($crem_details != null)
                            <li><a href="#deathdetails" data-toggle="tab">Death Details</a></li>
                        @endif
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="cremstatus">
                            @if($crem_details != null)
                                <div class="container">
                                    <div style="padding:10px;" class="pull-right"> 
                                        <b class="text-bts">Edit</b><button type="button" class="btn btn-function" data-toggle="modal" data-target="#mdl-edit-schedule"><i class="glyphicon glyphicon-pencil"></i></button>
                                    </div>
                                    <table class="table">
                                        <tr>
                                            <td style="width: 150px;"><label>Status</label></td>
                                            <td>
                                                @if($crem_details->status == "Cremation Service Completed")
                                                    <label class="c-completed">{{$crem_details->status}}</label><br>
                                                @elseif($crem_details->status =="Incomplete Requirements")
                                                    <label class="c-incomplete">{{$crem_details->status}}</label><br>
                                                @elseif($crem_details->status =="Initially Scheduled")
                                                    <label class="c-initially">{{$crem_details->status}}</label><br>
                                                @elseif($crem_details->status =="Schedule Confirmed")
                                                    <label class="c-confirmed">{{$crem_details->status}}</label><br>
                                                @elseif($crem_details->status =="Canceled")
                                                    <label class="c-canceled">{{$crem_details->status}}</label><br>
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><label>Scheduled Date</label></td>
                                            <td><span id="cDateString"></span><input type="hidden" id="cDatetoString" value="{{$crem_details->date_schedule}}"></td> 
                                        </tr>
                                        <tr>
                                            <td><label>Scheduled Time</label></td>
                                            <td>
                                                @if($chour == 00)
                                                    12 : {{$cmin}} : AM
                                                @elseif($chour > 12)
                                                    {{$chour - 12}} : {{$cmin}} PM
                                                @elseif($chour < 13)
                                                    {{$chour}} : {{$cmin}} : AM
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><label>Deceased Case</label></td>
                                            <td>
                                                @if($cbt->case != null)
                                                    {{$cbt->case}}
                                                    ( {{$cbt->no_days}} days )
                                                @else
                                                    ---
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><label>Deceased Built</label></td>
                                            <td>
                                                @if($cbt->built != null)
                                                    {{$cbt->built}}
                                                @else
                                                    ---
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><label>Deceased Type</label></td>
                                            <td>
                                                @if($cbt->type != null)
                                                    {{$cbt->type}}
                                                    ( {{$cbt->bone_type}} )
                                                @else
                                                    ---
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><label>Notes</label></td>
                                            <td>{{$crem_details->_notes}}</td>
                                        </tr>
                                    </table> 
                                </div>
                                @else
                                    <div class="container">
                                        <span class="text-center" style="padding: 10px;">
                                            <b href="" class="txt-bts">ADD DEATH DETAILS AND SET CREMATION SCHEDULE</b><button type="button" data-toggle="modal" data-target="#mdl-new-schedule" class="btn btn-function txt-bts"><i class="glyphicon glyphicon-plus"></i></button>
                                        </span>
                                    </div>
                                @endif 
                        </div>
                        <div class="tab-pane" id="payorinfo">
                            <div class="container">
                                @if($p_info != null)
                                    <div style="padding: 10px;" class="pull-right">
                                        <b class="text-bts">Edit</b><button type="button" class="btn btn-function" data-toggle="modal" data-target="#mdl-edit-payor"><i class="glyphicon glyphicon-pencil"></i></button>
                                    </div>
                                    <table class="table">
                                        <tr>
                                            <td style="width: 150px;"><label>Relation to Deceased</label></td>
                                            <td>{{$p_info->rel_to_deceased}}</td>
                                        </tr>
                                        <tr>
                                            <td><label>Full Name</label></td>
                                            <td>{{$p_info->l_name}} , {{$p_info->f_name}} {{$p_info->m_name}} @if($p_info->ext_name != null || $p_info->ext_name != "") ( {{$p_info->ext_name}} ) @endif</td>                                                            
                                        </tr>
                                        <tr>
                                            <td><label>Contact</label></td>
                                            <td>{{$p_info->contactnum}}</td>
                                        </tr>
                                        <tr>
                                            <td><label>Address</label></td>
                                            <td>
                                                @if($p_address !=null)
                                                    {{$p_address->province}} , {{$p_address->city_mun}}<br>
                                                    {{$p_address->brgy}} {{$p_address->details}}
                                                @else
                                                    ...
                                                @endif
                                            </td>
                                        </tr>
                                    </table>
                                @else
                                    <span style="padding: 10px;">
                                        <b href="" class="txt-bts">ADD PAYOR INFORMATION</b><button type="button" data-toggle="modal" data-target="#mdl-new-payor" class="btn btn-function txt-bts"><i class="glyphicon glyphicon-plus"></i></button>
                                    </span>                                    
                                @endif
                            </div>
                        </div>
                        @if($crem_details != null)
                            <div class="tab-pane" id="deathdetails">
                                <div class="container">
                                    <div style="padding: 10px;" class="pull-right">
                                        <b class="text-bts">Edit</b><button type="button" class="btn btn-function" data-toggle="modal" data-target="#mdl-edit-death-details"><i class="glyphicon glyphicon-pencil"></i></button>
                                    </div>

                                    <table class="table">
                                        <tr>
                                            <td style="width: 150px;"><label>Date of Death</label></td>
                                            <td><span id="dDateString"></span><input type="hidden" id="dDatetoString" value="{{$crem_details->date_death}}"></td>
                                        </tr>
                                        <tr>
                                            <td><label>Place of Death</label></td>
                                            <td>{{$crem_details->place_death}}</td>
                                        </tr>
                                        <tr>
                                            <td><label>Cause of Death</label></td>
                                            <td>{{$crem_details->cause_death}}</td>
                                        </tr>
                                    </table> 
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                    <!--///////// DOCUMENTS-->
                        <div class="box box-primary">
                            <div class="body-box">
                                <div class="container">
                                    <div style="padding: 10px;">
                                        <b class="text-bts">DOCUMENTS</b><button type="button" class="btn btn-function btn-edit" data-toggle="modal" data-target="#mdl-edit-documents"><i class="glyphicon glyphicon-pencil"></i></button>
                                    </div>
                                    <table class="table">
                                        <tr>
                                            <td style="width: 150px;"><label>Death Cert No.</label></td>
                                            <td>
                                                @if($docu == null || $docu->death_cert_no == null)
                                                    ---
                                                @else
                                                    {{$docu->death_cert_no}}
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><label>Transfer Permit No.</label></td>
                                            <td>
                                                @if($docu == null || $docu->trans_permit_no == null)
                                                    ---
                                                @else
                                                    {{$docu->trans_permit_no}}
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><label>Cremation Permit No.</label></td>
                                            <td>
                                                @if($docu == null || $docu->crem_permit_no == null)
                                                    ---
                                                @else
                                                    {{$docu->crem_permit_no}}
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><label>Exhumation Permit</label></td>
                                            <td>
                                                @if($docu == null || $docu->exhum_permit_no == null)
                                                    ---
                                                @else
                                                    {{$docu->exhum_permit_no}}
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><label>Senior Card No.</label></td>
                                            <td>
                                                @if($docu == null || $docu->senior_card_no == null)
                                                    ---
                                                @else
                                                    {{$docu->senior_card_no}}
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><label>Disability Card No.</label></td>
                                            <td>
                                                @if($docu == null || $docu->disabled_card_no == null)
                                                    ---
                                                @else
                                                    {{$docu->disabled_card_no}}
                                                @endif
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
                    
        </div>
               
    </section>
  </div>

<!--MODAL EDIT DECEASED-->
  <div id="mdl-edit-deceased" class="modal fade table-header">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <div class="text-center">
                    <span title="Beyond the Sunset" class="title" style="font-size:30px;">Beyond the Sunset</span>
                </div>
            </div>
            <div class="modal-body form-group-pad-body">
                <h4 class="modal-title text-center">Update Deceased Details</h4><br>
                <div id="status">
                </div>
                    <form class="thing-form" role="form" name="editdeceasedform" id="editdeceasedform" method="post" >
                    {{ csrf_field() }}
                    <input type="hidden" name="d_id" value="{{$show_info->id}}">
                    <input type="hidden" name="d_add_id" value="{{$d_address->id}}">
                        <div class="form-group has-feedback">
                                <div class="form-group">
                                    <label class="control-label float-left" for="name">Name</label>
                                    <div class="form-group form-group-pad">
                                        <input type="text" class="form-control form-control-pad" name="fname" value="{{$show_info->f_name}}">
                                        <input type="text" class="form-control form-control-pad" name="mname" value="{{$show_info->m_name}}">
                                        <input type="text" class="form-control form-control-pad" name="lname" value="{{$show_info->l_name}}">
                                        @if ($show_info->ext_name != null)
                                            <input type="text" class="form-control form-control-pad" name="ename" value="{{$show_info->ext_name}}">
                                        @else
                                            <input type="text" class="form-control form-control-pad" name="ename" placeholder="Suffix name (e.g. Jr, III) --optional">
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label float-left" for="gender" name="gender">Gender</label><br>
                                    <div class="form-group-pad">
                                        @if($show_info->gender == 'f')
                                            <input type="radio" class="form-control-pad" name="rd-gender" value="f" checked> Female
                                            <input type="radio" class="form-control-pad" name="rd-gender" value="m" style="margin-left: 10px;"> Male
                                        @else
                                            <input type="radio" class="form-control-pad" name="rd-gender" value="f" > Female
                                            <input type="radio" class="form-control-pad" name="rd-gender" value="m" style="margin-left: 10px;" checked> Male
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label float-left" for="age">Age</label><br>
                                    <input type="number" id="age" name="age" value="{{$show_info->age}}">
                                </div>
                                <div class="form-group">
                                    <label class="control-label float-left" for="citizenship">Address</label><br>
                                    <div class="form-group-pad">
                                        <input type="text" name="province" class="form-control form-control-pad" value="{{$d_address->province}}">
                                        <input type="text" name="city" class="form-control form-control-pad" value="{{$d_address->city_mun}}">
                                        <input type="text" name="brgy" class="form-control form-control-pad" value="{{$d_address->brgy}}">
                                        @if($d_address != null)
                                            <textarea name="details" class="form-control form-control-pad">{{$d_address->details}}</textarea>
                                        @else
                                            <textarea name="details" class="form-control form-control-pad" placeholder="Details"></textarea>
                                        @endif
                                    </div>
                                </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-9" style="text-align: right;"><a class="btn btn-lg" data-dismiss="modal">Cancel</a></div>
                            <div class="col-md-3">
                                <button type="button" class="btn btn-block btn-bts btn-lg" onclick="$(this).sendInfo('{{route('deceased.updateDeceased')}}', '#editdeceasedform', 'mdl-edit-deceased');"> Update</button>
                            </div>
                        </div>
                    </form>
            </div>
        </div>
    </div>
  </div>

<!--MODAL NEW PAYOR-->
  <div id="mdl-new-payor" class="modal fade table-header">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <div class="text-center">
                    <span title="Beyond the Sunset" class="title" style="font-size:30px;">Beyond the Sunset</span>
                </div>
            </div>
            <div class="modal-body form-group-pad-body">
                <h4 class="modal-title text-center">Add Payor Details for {{$show_info->f_name}} {{$show_info->l_name}}</h4><br>
                <div id="status">
                </div>
                    <form class="thing-form" role="form" name="newpayorform" id="newpayorform" method="post" >
                    {{ csrf_field() }}
                        <div class="form-group has-feedback">
                                <input type="hidden" id="d_id" name="d_id" value="{{$show_info->id}}">

                                <div class="form-group">
                                    <label class="control-label float-left" for="relationtodeceased">Relation to {{$show_info->f_name}}</label>
                                    <div class="form-group form-group-pad">
                                        <input type="text" class="form-control form-control-pad" name="relationtodeceased" placeholder="Relation to Deceased">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label float-left" for="name">Name</label>
                                    <div class="form-group form-group-pad">
                                        <input type="text" class="form-control form-control-pad" name="fname" placeholder="First name">
                                        <input type="text" class="form-control form-control-pad" name="mname" placeholder="Middle name">
                                        <input type="text" class="form-control form-control-pad" name="lname" placeholder="Last name">
                                        <input type="text" class="form-control form-control-pad" name="ename" placeholder="Suffix name (e.g. Jr, III) --optional">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label float-left" for="contactnum">Contact #</label><br>
                                    <div class="form-group-pad">
                                        <input type="text" name="contactnum" class="form-control" placeholder="09--">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label float-left" for="citizenship">Address</label><br>
                                    <div class="form-group-pad">
                                        <input type="text" name="province" class="form-control form-control-pad" placeholder="Province">
                                        <input type="text" name="city" class="form-control form-control-pad" placeholder="City / Municipality">
                                        <input type="text" name="brgy" class="form-control form-control-pad" placeholder="Barangay">
                                        <textarea name="details" class="form-control form-control-pad" placeholder="Details"></textarea>
                                    </div>
                                </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-9" style="text-align: right;"><a class="btn btn-lg" data-dismiss="modal">Cancel</a></div>
                            <div class="col-md-3">
                                <button type="button" class="btn btn-block btn-bts btn-lg" onclick="$(this).sendInfo('{{route('deceased.sendPayor')}}', '#newpayorform', '#mdl-new-payor');" > Add</button>
                                                                                                                    <!--ROUTE                  FORM                  MODAL-->
                            </div>
                        </div>
                    </form>
            </div>
        </div>
    </div>
  </div>

<!--MODAL EDIT PAYOR-->
  @if($p_info != null)
  <div id="mdl-edit-payor" class="modal fade table-header">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <div class="text-center">
                    <span title="Beyond the Sunset" class="title" style="font-size:30px;">Beyond the Sunset</span>
                </div>
            </div>
            <div class="modal-body form-group-pad-body">
                <h4 class="modal-title text-center">Update Payor Details for {{$show_info->f_name}} {{$show_info->l_name}}</h4><br>
                <div id="status">
                </div>
                    <form class="thing-form" role="form" name="editpayorform" id="editpayorform" method="post" >
                    {{ csrf_field() }}
                    <input type="hidden" name="d_id" value="{{$show_info->id}}">
                    <input type="hidden" name="p_add_id" value="{{$p_address->id}}">
                        <div class="form-group has-feedback">
                                <div class="form-group">
                                    <label class="control-label float-left" for="relationtodeceased">Relation to {{$show_info->f_name}}</label>
                                    <div class="form-group form-group-pad">
                                        <input type="text" class="form-control form-control-pad" name="relationtodeceased" value="{{$p_info->rel_to_deceased}}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label float-left" for="name">Name</label>
                                    <div class="form-group form-group-pad">
                                        <input type="text" class="form-control form-control-pad" name="fname" value="{{$p_info->f_name}}">
                                        <input type="text" class="form-control form-control-pad" name="mname" value="{{$p_info->m_name}}">
                                        <input type="text" class="form-control form-control-pad" name="lname" value="{{$p_info->l_name}}">
                                        @if ($p_info->ext_name != null)
                                            <input type="text" class="form-control form-control-pad" name="ename" value="{{$p_info->ext_name}}">
                                        @else
                                            <input type="text" class="form-control form-control-pad" name="ename" placeholder="Suffix name (e.g. Jr, III) --optional">
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label float-left" for="contactnum">Contact #</label><br>
                                    <div class="form-group-pad">
                                        <input type="text" name="contactnum" class="form-control" value="{{$p_info->contactnum}}">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="control-label float-left" for="citizenship">Address</label><br>
                                    <div class="form-group-pad">
                                        <input type="text" name="province" class="form-control form-control-pad" value="{{$p_address->province}}">
                                        <input type="text" name="city" class="form-control form-control-pad" value="{{$p_address->city_mun}}">
                                        <input type="text" name="brgy" class="form-control form-control-pad" value="{{$p_address->brgy}}">
                                        @if($p_address != null)
                                            <textarea name="details" class="form-control form-control-pad">{{$p_address->details}}</textarea>
                                        @else
                                            <textarea name="details" class="form-control form-control-pad" placeholder="Details"></textarea>
                                        @endif
                                    </div>
                                </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-9" style="text-align: right;"><a class="btn btn-lg" data-dismiss="modal">Cancel</a></div>
                            <div class="col-md-3">
                                <button type="button" class="btn btn-block btn-bts btn-lg" onclick="$(this).sendInfo('{{route('deceased.updatePayor')}}', '#editpayorform', '#mdl-edit-payor');"> Update</button>
                            </div>
                        </div>
                    </form>
            </div>
        </div>
    </div>
  </div>
  @endif
<!--MODAL NEW SCHEDULE-->
    <div id="mdl-new-schedule" class="modal fade table-header">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <div class="text-center">
                        <span title="Beyond the Sunset" class="title" style="font-size:30px;">Beyond the Sunset</span>
                    </div>
                </div>
                <div class="modal-body form-group-pad-body">
                    <h4 class="modal-title text-center">Add Death details and Schedule a Cremation for {{$show_info->f_name}} {{$show_info->l_name}}</h4><br>
                    <div class="text-center" style="margin-bottom: 20px;">Current Status: 
                        <label id="lblStatus"></label>
                    </div>
                    <div id="status">
                    </div>
                        <form class="thing-form" role="form" name="newscheduleform" id="newscheduleform" method="post" >
                        {{ csrf_field() }}
                            <div class="form-group">
                                <div class="col-sm-6">
                                    
                                    <div class="form-group">
                                        <label class="control-label float-left" for="newdeathDate">Date of Death</label><br>
                                        <div class="form-group-pad">
                                            <select class="dmonth form-control date-pad inline-block newddate">
                                                <option value="" disabled selected>Month</option>
                                                <option value="01">January</option>
                                                <option value="02">February</option>
                                                <option value="03">March</option>
                                                <option value="04">April</option>
                                                <option value="05">May</option>
                                                <option value="06">June</option>
                                                <option value="07">July</option>
                                                <option value="08">August</option>
                                                <option value="09">September</option>
                                                <option value="10">October</option>
                                                <option value="11">November</option>
                                                <option value="12">December</option>
                                            </select>
                                                <input type="number" id="dday" class="dday form-control form-control-pad newddate" placeholder="Day">
                                                <input type="number" id="dyear" class="dyear form-control form-control-pad newddate" placeholder="Year">
                                        </div>
                                        <input type="hidden" class="newdeathDate" name="newdeathDate" value="">
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label float-left" for="deathPlace">Place of Death</label><br>
                                        <div class="form-group-pad">
                                            <input type="text" class="form-control form-control-pad" name="deathPlace" placeholder="Place of Death">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label float-left" for="deathCause">Cause of Death</label><br>
                                        <div class="formm-group-pad">
                                            <textarea name="deathCause" class="form-control form-control-pad" placeholder="Cause of Death"></textarea>
                                        </div>
                                    </div>
                                    
                                </div>
                                <div class="col-sm-6">
                                    <input type="hidden" id="d_id" name="d_id" value="{{$show_info->id}}">

                                    <div class="form-group">
                                        <label class="control-label float-left" for="newscheduleDate">Date of Cremation</label><br>
                                        <div class="form-group-pad">
                                            <select class="form-control date-pad inline-block newcremationdate newcremationmonth">
                                                <option value="" disabled selected>Month</option>
                                                <option value="01">January</option>
                                                <option value="02">February</option>
                                                <option value="03">March</option>
                                                <option value="04">April</option>
                                                <option value="05">May</option>
                                                <option value="06">June</option>
                                                <option value="07">July</option>
                                                <option value="08">August</option>
                                                <option value="09">September</option>
                                                <option value="10">October</option>
                                                <option value="11">November</option>
                                                <option value="12">December</option>
                                            </select>
                                                <input type="number" class="form-control form-control-pad newcremationdate newcremationday" placeholder="Day">
                                                <input type="number" class="form-control form-control-pad newcremationdate newcremationyear" placeholder="Year">
                                        </div>
                                        <input type="hidden" class="newscheduleDate" name="newscheduleDate" value="">
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label float-left" for="newcremationtime">Time of Cremation</label>
                                        <div class="form-group form-group-pad">
                                            <input type="number" class="form-control form-control-pad hours newcremationtime newcremationhour" max="12" placeholder="HH">
                                            <input type="number" class="form-control form-control-pad minutes newcremationtime newcremationmin" placeholder="MM">
                                            <select class="newcremationtime form-control form-control-pad newcremationampm">
                                                <option value="" disabled selected></option>
                                                <option value="am">AM</option>
                                                <option value="pm">PM</option>
                                            </select>
                                        </div>
                                        <input type="hidden" class="newscheduleTime" name="newscheduleTime" value="">
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label float-left" for="notes" name="notes">Additional Notes</label><br>
                                        <div class="form-group-pad">
                                            <textarea name="notes" class="form-control form-control-pad" placeholder="Additional Notes"></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label float-left" for="notes" name="status">Status</label><br>
                                        <div class="form-group-pad">
                                            <select name="status" id="crem_status" class="form-control date-pad inline-block cstatus">
                                                <option value="" disabled selected>Status</option>
                                                <option value="Incomplete Requirements">Incomplete Requirements</option>
                                                <option value="Initially Scheduled">Initially Scheduled</option>
                                                <option value="Schedule Confirmed">Schedule Confirmed</option>
                                                <option value="Cremation Service Completed">Cremation Service Completed</option>
                                                <option value="Canceled">Canceled</option>
                                            </select>
                                        </div>
                                    </div>

                                </div>

                                
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-9" style="text-align: right;"><a class="btn btn-lg" data-dismiss="modal">Cancel</a></div>
                                <div class="col-md-3">
                                    <button type="button" class="btn btn-block btn-bts btn-lg" onclick="$(this).sendInfo('{{route('deceased.sendCremDetails')}}', '#newscheduleform', '#mdl-new-schedule');"> Add</button>
                                </div>
                            </div>
                        </form>
                </div>
            </div>
        </div>
    </div>

<!--MODAL EDIT SCHEDULE-->
    <div id="mdl-edit-schedule" class="modal fade table-header">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <div class="text-center">
                        <span title="Beyond the Sunset" class="title" style="font-size:30px;">Beyond the Sunset</span>
                    </div>
                </div>
                @if($crem_details != null)
                <div class="modal-body form-group-pad-body">
                    <h4 class="modal-title text-center">Update Schedule of Cremation for {{$show_info->f_name}} {{$show_info->l_name}}</h4><br>
                        <div class="text-center" style="margin-bottom: 20px;">
                        Current Status: 
                                                @if($crem_details->status == "Cremation Service Completed")
                                                    <label class="c-completed">{{$crem_details->status}}</label><br>
                                                @elseif($crem_details->status =="Incomplete Requirements")
                                                    <label class="c-incomplete">{{$crem_details->status}}</label><br>
                                                @elseif($crem_details->status =="Initially Scheduled")
                                                    <label class="c-initially">{{$crem_details->status}}</label><br>
                                                @elseif($crem_details->status =="Schedule Confirmed")
                                                    <label class="c-confirmed">{{$crem_details->status}}</label><br>
                                                @elseif($crem_details->status =="Canceled")
                                                    <label class="c-canceled">{{$crem_details->status}}</label><br>
                                                @endif
                            <label id="lblStatus"></label>
                        </div>
                    <div id="status"></div>
                        <form class="thing-form" role="form" name="editscheduleform" id="editscheduleform" method="post" >
                        {{ csrf_field() }}
                            <div class="form-group">
                                <input type="hidden" id="d_id" name="d_id" value="{{$show_info->id}}">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label float-left" for="editscheduleDate">Date of Cremation</label><br>
                                        <div class="form-group-pad">
                                            <select class="form-control date-pad inline-block editcremationdate editcremationmonth">
                                                <option value="" disabled selected>Month</option>
                                                <option value="01" {{ ($cmonth == '01' ? "selected":"") }} >January</option>
                                                <option value="02" {{ ($cmonth == '02' ? "selected":"") }} >February</option>
                                                <option value="03" {{ ($cmonth == '03' ? "selected":"") }} >March</option>
                                                <option value="04" {{ ($cmonth == '04' ? "selected":"") }} >April</option>
                                                <option value="05" {{ ($cmonth == '05' ? "selected":"") }} >May</option>
                                                <option value="06" {{ ($cmonth == '06' ? "selected":"") }} >June</option>
                                                <option value="07" {{ ($cmonth == '07' ? "selected":"") }} >July</option>
                                                <option value="08" {{ ($cmonth == '08' ? "selected":"") }} >August</option>
                                                <option value="09" {{ ($cmonth == '09' ? "selected":"") }} >September</option>
                                                <option value="10" {{ ($cmonth == '10' ? "selected":"") }} >October</option>
                                                <option value="11" {{ ($cmonth == '11' ? "selected":"") }} >November</option>
                                                <option value="12" {{ ($cmonth == '12' ? "selected":"") }} >December</option>
                                            </select>
                                            @if($cday != null)
                                                <input type="number" class="form-control form-control-pad editcremationdate editcremationday" value="{{$cday}}">
                                            @else
                                                <input type="number" class="form-control form-control-pad editcremationdate editcremationday" placeholder="Day">
                                            @endif

                                            @if($cyear != null)
                                                <input type="number" class="form-control form-control-pad editcremationdate editcremationyear" value="{{$cyear}}">
                                            @else
                                                <input type="number" class="form-control form-control-pad editcremationdate editcremationyear" placeholder="Year">
                                            @endif
                                        </div>
                                        <input type="hidden" class="editscheduleDate" name="editscheduleDate" value="{{$crem_details->date_schedule}}">
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label float-left" for="editscheduleTime">Time of Cremation</label>
                                        <div class="form-group form-group-pad">
                                        @if($chour > 12)
                                            <input type="number" class="form-control form-control-pad hours editcremationtime editcremationhour" max="12" value="{{$chour - 12}}">
                                        @else
                                            <input type="number" class="form-control form-control-pad hours editcremationtime editcremationhour" max="12" value="{{$chour}}">
                                        @endif
                                            <input type="number" class="form-control form-control-pad minutes editcremationtime editcremationmin" value="{{$cmin}}">
                                            <select name="ampm" class="form-control form-control-pad editcremationtime editcremationampm">
                                                <option value="" disabled selected></option>
                                                <option value="am" {{ ($chour < 13 ? "selected":"") }} >AM</option>
                                                <option value="pm" {{ ($chour > 12 ? "selected":"") }} >PM</option>
                                            </select>
                                        </div>
                                        <input type="hidden" class="editscheduleTime" name="editscheduleTime" value="{{$crem_details->time_schedule}}">
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label float-left" for="notes" name="notes">Additional Notes</label><br>
                                        <div class="form-group-pad">
                                            <textarea name="notes" class="form-control form-control-pad">{{$crem_details->_notes}}</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label float-left" for="notes" name="editcremstatus">Status</label><br>
                                        <div class="form-group-pad">
                                            <select name="editcremstatus" id="editcrem_status" class="form-control date-pad inline-block cstatus">
                                                <option value="" disabled selected>Status</option>
                                                <option value="Incomplete Requirements" {{ ($crem_details->status == 'Incomplete Requirements' ? "selected":"") }}>Incomplete Requirements</option>
                                                <option value="Initially Scheduled" {{ ($crem_details->status == 'Initially Scheduled' ? "selected":"") }} >Initially Scheduled</option>
                                                <option value="Schedule Confirmed" {{ ($crem_details->status == 'Schedule Confirmed' ? "selected":"") }} >Schedule Confirmed</option>
                                                <option value="Cremation Service Completed" {{ ($crem_details->status == 'Cremation Service Completed' ? "selected":"") }} >Cremation Service Completed</option>
                                                <option value="Canceled" {{ ($crem_details->status == 'Canceled' ? "selected":"") }}> Canceled</option>
                                            </select>
                                            <p style="color: red;" id="settoarchive"></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label float-left" for="dcase" >Deceased Case</label><br>
                                         <div class="form-group-pad">
                                            @if($cbt->case != null)
                                                <input type="radio" value="Fresh Body" class="form-control-pad" {{ ($cbt->case == 'Fresh Body' ? "checked":"") }}> Fresh Body
                                                <input type="radio" value="Embalmed" class="form-control-pad" style="margin-left: 10px;" {{ ($cbt->case == 'Embalmed' ? "checked":"") }} > Embalmed
                                                <input type="text" name="nodays" class="form-control form-control-pad" style="margin-left: 10px;" value="{{$cbt->no_days}}">
                                                <input type="hidden" name="dcase" class="dcase" value="{{$cbt->case}}">
                                            @else
                                                <input type="radio" class="form-control-pad" name="dcasecheck" value="Fresh Body" > Fresh Body
                                                <input type="radio" class="form-control-pad" name="dcasecheck" style="margin-left: 10px;" value="Embalmed"> Embalmed
                                                <input type="text" name="nodays" class="form-control form-control-pad" style="margin-left: 10px;" placeholder="No. of days">
                                                <input type="hidden" name="dcase" class="dcase" value="">
                                            @endif
                                            
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label float-left" for="dbuilt" >Deceased Built</label><br>
                                         <div class="form-group-pad">
                                            @if($cbt->built != null)
                                                <input type="radio" class="form-control-pad" value="Light" {{ ($cbt->built == 'Light' ? "checked":"") }}> Light
                                                <input type="radio" class="form-control-pad" value="Medium" style="margin-left: 10px;" {{ ($cbt->built == 'Medium' ? "checked":"") }} > Medium
                                                <input type="radio" class="form-control-pad" value="Heavy" style="margin-left: 10px;" {{ ($cbt->built == 'Heavy' ? "checked":"") }} > Heavy
                                                <input type="radio" class="form-control-pad" value="Overweight" style="margin-left: 10px;" {{ ($cbt->built == 'Overweight' ? "checked":"") }} > Overweight
                                                <input type="hidden" name="dbuilt" class="dbuilt" value="{{$cbt->built}}">
                                            @else
                                                <input type="radio" class="form-control-pad" name="dbuiltcheck" value="Light"> Light
                                                <input type="radio" class="form-control-pad" name="dbuiltcheck" value="Medium" style="margin-left: 10px;"> Medium
                                                <input type="radio" class="form-control-pad" name="dbuiltcheck" value="Heavy" style="margin-left: 10px;"> Heavy
                                                <input type="radio" class="form-control-pad" name="dbuiltcheck" value="Overweight" style="margin-left: 10px;"> Overweight
                                                <input type="hidden" name="dbuilt" class="dbuilt" value="">
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label float-left" for="dbuilt" >Deceased Type</label><br>
                                         <div class="form-group-pad">
                                            @if($cbt->type != null)
                                                <div style="margin-bottom: 10px;">
                                                    <input type="radio" class="form-control-pad" value="Infant" {{ ($cbt->type == 'Infant' ? "checked":"") }}> Infant
                                                    <input type="radio" class="form-control-pad" value="Child" style="margin-left: 10px;" {{ ($cbt->type == 'Child' ? "checked":"") }} > Child
                                                    <input type="radio" class="form-control-pad" value="Adult" style="margin-left: 10px;" {{ ($cbt->type == 'Adult' ? "checked":"") }} > Adult
                                                    <input type="hidden" name="dtype" class="dtype" value="{{$cbt->type}}">
                                                </div>
                                                <div>
                                                    <input type="radio" class="form-control-pad" value="Bone" {{ ($cbt->bone_type == 'Bone' ? "checked":"") }}> Bone
                                                    <input type="radio" class="form-control-pad" value="Semi-bone" style="margin-left: 10px;" {{ ($cbt->bone_type == 'Semi-bone' ? "checked":"") }} > Semi-bone
                                                    <input type="hidden" name="dbonetype" class="dbonetype" value="{{$cbt->bone_type}}">
                                                </div>
                                            @else
                                                <div style="margin-bottom: 10px;">
                                                    <input type="radio" class="form-control-pad" name="dtypecheck" value="Infant"> Infant
                                                    <input type="radio" class="form-control-pad" name="dtypecheck" value="Child" style="margin-left: 10px;"> Child
                                                    <input type="radio" class="form-control-pad" name="dtypecheck" value="Adult" style="margin-left: 10px;"> Adult
                                                    <input type="hidden" name="dtype" class="dtype" value="">
                                                </div>
                                                <div>
                                                    <input type="radio" class="form-control-pad" name="dbonetypecheck" value="Bone"> Bone
                                                    <input type="radio" class="form-control-pad" name="dbonetypecheck" value="Semi-bone" style="margin-left: 10px;"> Semi-bone
                                                    <input type="hidden" name="dbonetype" class="dbonetype" value="">
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-9" style="text-align: right;"><a class="btn btn-lg" data-dismiss="modal">Cancel</a></div>
                                <div class="col-md-3">
                                    <button type="button" class="btn btn-block btn-bts btn-lg" onclick="$(this).sendInfo('{{route('deceased.updateCremDetails')}}', '#editscheduleform', '#mdl-edit-schedule');"> Update</button>
                                </div>
                            </div>
                        </form>
                </div>
                @endif
            </div>
        </div>
    </div>

<!--MODAL EDIT DEATH DETAILS-->
    <div id="mdl-edit-death-details" class="modal fade table-header">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <div class="text-center">
                        <span title="Beyond the Sunset" class="title" style="font-size:30px;">Beyond the Sunset</span>
                    </div>
                </div>
                @if($crem_details != null)
                <div class="modal-body form-group-pad-body">
                    <h4 class="modal-title text-center">Edit Death details for {{$show_info->f_name}} {{$show_info->l_name}}</h4><br>
                    <div class="text-center" style="margin-bottom: 20px;">Current Status: 
                        <label id="lblStatus"></label>
                    </div>
                    <div id="status"></div>
                        <form class="thing-form" role="form" name="editdeathdetailsform" id="editdeathdetailsform" method="post" >
                        {{ csrf_field() }}
                        <input type="hidden" name="d_id" value="{{$show_info->id}}">
                            <div class="form-group">
                                    <div class="form-group">
                                        <label class="control-label float-left" for="deathDate">Date of Death</label><br>
                                        <div class="form-group-pad">
                                            <select class="form-control date-pad inline-block deathdate deathmonth">
                                                <option value="" disabled selected>Month</option>
                                                <option value="01" {{ ($dmonth == '01' ? "selected":"") }} >January</option>
                                                <option value="02" {{ ($dmonth == '02' ? "selected":"") }} >February</option>
                                                <option value="03" {{ ($dmonth == '03' ? "selected":"") }} >March</option>
                                                <option value="04" {{ ($dmonth == '04' ? "selected":"") }} >April</option>
                                                <option value="05" {{ ($dmonth == '05' ? "selected":"") }} >May</option>
                                                <option value="06" {{ ($dmonth == '06' ? "selected":"") }} >June</option>
                                                <option value="07" {{ ($dmonth == '07' ? "selected":"") }} >July</option>
                                                <option value="08" {{ ($dmonth == '08' ? "selected":"") }} >August</option>
                                                <option value="09" {{ ($dmonth == '09' ? "selected":"") }} >September</option>
                                                <option value="10" {{ ($dmonth == '10' ? "selected":"") }} >October</option>
                                                <option value="11" {{ ($dmonth == '11' ? "selected":"") }} >November</option>
                                                <option value="12" {{ ($dmonth == '12' ? "selected":"") }} >December</option>
                                            </select>
                                            @if($dday != null)
                                                <input type="number" class="form-control form-control-pad deathdate deathday" value="{{$dday}}">
                                            @else
                                                <input type="number" class="form-control form-control-pad deathdate deathday" placeholder="Day">
                                            @endif

                                            @if($dyear != null)
                                                <input type="number" class="form-control form-control-pad deathdate deathyear" value="{{$dyear}}">
                                            @else
                                                <input type="number" class="form-control form-control-pad deathdate deathyear" placeholder="Year">
                                            @endif
                                        </div>
                                        <input type="hidden" class="deathDate" name="deathDate" value="{{$crem_details->date_death}}">
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label float-left" for="deathPlace">Place of Death</label><br>
                                        <div class="form-group-pad">
                                            <input type="text" class="form-control form-control-pad" name="deathPlace" value="{{$crem_details->place_death}}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label float-left" for="deathCause">Cause of Death</label><br>
                                        <div class="formm-group-pad">
                                            <textarea name="deathCause" class="form-control form-control-pad">{{$crem_details->cause_death}}</textarea>
                                        </div>
                                    </div>
                                    
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-9" style="text-align: right;"><a class="btn btn-lg" data-dismiss="modal">Cancel</a></div>
                                <div class="col-md-3">
                                    <button type="button" class="btn btn-block btn-bts btn-lg" onclick="$(this).sendInfo('{{route('deceased.updateDeathDetails')}}', '#editdeathdetailsform', '#mdl-edit-death-details');"> Update</button>
                                </div>
                            </div>
                        </form>
                </div>
                @endif
            </div>
        </div>
    </div>
<!--MODAL EDIT DOCUMENTS-->
    <div id="mdl-edit-documents" class="modal fade table-header">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <div class="text-center">
                        <span title="Beyond the Sunset" class="title" style="font-size:30px;">Beyond the Sunset</span>
                    </div>
                </div>
                <div class="modal-body form-group-pad-body">
                    <h4 class="modal-title text-center">Update documents for {{$show_info->f_name}} {{$show_info->l_name}}</h4><br>
                    <div id="status"></div>
                        <form class="thing-form" role="form" name="editdocumentsform" id="editdocumentsform" method="post" >
                        {{ csrf_field() }}
                        <input type="hidden" name="d_id" value="{{$show_info->id}}">
                            <div class="form-group">
                                <div class="form-group">
                                    <label class="control-label float-left" for="deathcertno">Death Certificate number</label> (No Death Cert, No Cremation)<br>
                                    <div class="form-group-pad">
                                        @if($docu != null)
                                            <input type="text" class="form-control form-control-pad" name="deathcertno" value="{{$docu->death_cert_no}}">
                                        @else
                                            <input type="text" class="form-control form-control-pad" name="deathcertno">
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label float-left" for="transpermitno">Transfer Permit number</label> (From Health Office where death occured)<br>
                                    <div class="form-group-pad">
                                        @if($docu != null)
                                            <input type="text" class="form-control form-control-pad" name="transpermitno" value="{{$docu->trans_permit_no}}">
                                        @else
                                            <input type="text" class="form-control form-control-pad" name="transpermitno">
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label float-left" for="crempermitno">Cremation Permit number</label> (Acquired from Tuba Municipal Health Office)<br>
                                    <div class="form-group-pad">
                                        @if($docu != null)
                                            <input type="text" class="form-control form-control-pad" name="crempermitno" value="{{$docu->crem_permit_no}}">
                                        @else
                                            <input type="text" class="form-control form-control-pad" name="crempermitno">
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label float-left" for="exhumpermitno">Exhumation Permit</label> (From Health office of burial place for Bone cremations)<br>
                                    <div class="form-group-pad">
                                        @if($docu != null)
                                            <input type="text" class="form-control form-control-pad" name="exhumpermitno" value="{{$docu->exhum_permit_no}}">
                                        @else
                                            <input type="text" class="form-control form-control-pad" name="exhumpermitno">
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label float-left" for="seniorcardno">Senior Card number</label> (Senior Citizen's Discount)<br>
                                    <div class="form-group-pad">
                                        @if($docu != null)
                                            <input type="text" class="form-control form-control-pad" name="seniorcardno" value="{{$docu->senior_card_no}}">
                                        @else
                                            <input type="text" class="form-control form-control-pad" name="seniorcardno">
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label float-left" for="disabcardno">Disability Card number</label> (Disability Discount)<br>
                                    <div class="form-group-pad">
                                        @if($docu != null)
                                            <input type="text" class="form-control form-control-pad" name="disabcardno" value="{{$docu->disabled_card_no}}">
                                        @else
                                            <input type="text" class="form-control form-control-pad" name="disabcardno">
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-9" style="text-align: right;"><a class="btn btn-lg" data-dismiss="modal">Cancel</a></div>
                                <div class="col-md-3">
                                    <button type="button" class="btn btn-block btn-bts btn-lg" onclick="$(this).sendInfo('{{route('deceased.updateDocuments')}}', '#editdocumentsform', '#mdl-edit-documents');"> Update</button>
                                </div>
                            </div>
                        </form>
                </div>
            </div>
        </div>
    </div>


@stop

@section('plugins-css')
    <link rel="stylesheet" href="{{asset('adminlte')}}/plugins/datatables/dataTables.bootstrap.css">
@stop

@section('plugins-script')
    <script src="{{asset('adminlte')}}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{{asset('adminlte')}}/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script src="{{asset('adminlte')}}/plugins/daterangepicker/moment.js"></script>
    <script type="text/javascript">

        // Convert data to words
            var crem_date = $('#cDatetoString').val(); cdate = moment(crem_date).format('MMMM DD, YYYY');
            // var dece_date = $('#pDatetoString').val(); pdate = moment(dece_date).format('MMMM DD, YYYY');
            // var guar_date = $('#gDatetoString').val(); gdate = moment(guar_date).format('MMMM DD, YYYY');
            var deat_date = $('#dDatetoString').val(); ddate = moment(deat_date).format('MMMM DD, YYYY');
            $('#cDateString').text(cdate); $('#dDateString').text(ddate);
        
        // AJAX to send data
            $.fn.sendInfo = function(rout,  formid, modalid){
                $.ajax({
                    type : 'POST',
                    url : rout,
                    data: $(formid).serialize(),
                    dataType : 'json',
                    error : function(){
                        alert('error');
                    },
                    success : function(data){
                        console.log(data);
                        var errors = '';
                if(data['status']==0){
                    for(var key in data['errors']){
                        errors += data['errors'][key]+'<br />';
                    }
                    $(modalid + ' .modal-body  #status').html('<div class="alert alert-danger alert-dismissible"><h4><i class="icon fa fa-ban"></i> Alert!</h4>'+errors+'</div>').fadeIn();
                }else if(data['status'] == 2){
                    $(modalid + ' .modal-body  #status').html('<div class="alert alert-success alert-dismissible"><h4><i class="icon fa fa-ban"></i> Archived!</h4>'+errors+'</div>').fadeIn().delay(1500).fadeOut(100);
                    location.href = "{{route('deceased.index')}}";
                }else{
                    $(modalid + ' .modal-body  #status').html('<div class="alert alert-success alert-dismissible"><h4><i class="icon fa fa-ban"></i> Success!</h4>'+errors+'</div>').fadeIn().delay(1500).fadeOut(100);
                    location.reload();
                }
                
                    }
                });
            };
        // Date for death
            $('.newddate').on('blur', function(){
                var newdday = $('.dday').val();
                var newdmonth = $('.dmonth').val();
                var newdyear = $('.dyear').val();

                setDate(newdmonth, newdday, newdyear, '.dday', '.dyear', '.newdeathDate');

            });

            $('.deathdate').on('blur', function(){
                var dday = $('.deathday').val();
                var dmonth = $('.deathmonth').val();
                var dyear = $('.deathyear').val();

                setDate(dmonth, dday, dyear, '.deathday', '.deathyear', '.deathDate');

            });

        // Date for schedule
            $('.newcremationdate').on('blur', function(){
                var newcremday = $('.newcremationday').val();
                var newcremmonth = $('.newcremationmonth').val();
                var newcremyear = $('.newcremationyear').val();

                setDate(newcremmonth, newcremday, newcremyear, '.newcremationday', '.newcremationyear', '.newscheduleDate');

            });

            $('.editcremationdate').on('blur', function(){
                var editcremday = $('.editcremationday').val();
                var editcremmonth = $('.editcremationmonth').val();
                var editcremyear = $('.editcremationyear').val();

                setDate(editcremmonth, editcremday, editcremyear, '.editcremationday', '.editcremationyear', '.editscheduleDate');
            });

        // Time for schedule
            $('.newcremationtime').on('blur', function(){
                var hour = $('.newcremationhour').val();
                var min = $('.newcremationmin').val();
                var ampm = $('.newcremationampm').val();
                var hourmin = parseInt($('.newcremationhour').val());
                
                if(hour != null && min != null && ampm != null){
                    if(hour.length == 1){
                        hour = '0' + hour;
                        $('.newcremationhour').val(hour);
                    }
                    if(min.length == 1){
                        min = '0' + min;
                        $('.newcremationmin').val(min);
                    }
                    var checkTime = moment(hour+':'+min+' '+ampm, 'hh:mm aA').isValid();
                    if(checkTime){
                        if(ampm == 'pm'){
                            if(hourmin == 12){
                                hourmin = 00;
                            }
                            else{
                                hourmin += 12;
                            }
                        }
                        else{
                            hourmin = '0' + hourmin;
                        }
                        $('.newscheduleTime').val(hourmin+':'+min+':'+'00');
                    }
                }
            });

            $('.editcremationtime').on('blur', function(){
                var hour = $('.editcremationhour').val();
                var min = $('.editcremationmin').val();
                var ampm = $('.editcremationampm').val();
                var hourmin = parseInt($('.editcremationhour').val());
                
                if(hour != null && min != null && ampm != null){
                    if(hour.length == 1){
                        hour = '0' + hour;
                        $('.editcremationhour').val(hour);
                    }
                    if(min.length == 1){
                        min = '0' + min;
                        $('.editcremationmin').val(min);
                    }
                    var checkTime = moment(hour+':'+min+' '+ampm, 'hh:mm aA').isValid();
                    if(checkTime){
                        if(ampm == 'pm'){
                            if(hourmin == 12){
                                hourmin = 00;
                            }
                            else{
                                hourmin += 12;
                            }
                        }
                        else{
                            hourmin = '0' + hourmin;
                        }
                        $('.editscheduleTime').val(hourmin+':'+min+':'+'00');
                    }
                }
            });

        // monitor status change
            $('#crem_status').on('change', function(){
                var status = $(this).val();
                
                if(status == "Incomplete Requirements"){
                    $('#lblStatus').html("<label class='c-incomplete'>Incomplete Requirements</label>");
                    $('#settoarchive').text('');
                }
                else if(status == "Initially Scheduled"){
                    $('#lblStatus').html("<label class='c-initially: #342897;'>Initially Scheduled</label>");
                    $('#settoarchive').text('');
                }
                else if(status == "Schedule Confirmed"){
                    $('#lblStatus').html("<label style='c-confirmed;'>Schedule Confirmed</label>");
                    $('#settoarchive').text('');
                }
                else if(status == "Cremation Service Completed"){
                    $('#lblStatus').html("<label style='c-completed'>Cremation Service Completed</label>");
                    $('#settoarchive').text('Deceased details will be moved to Archives.');
                }
                else if(status == "Canceled"){
                    $('#lblStatus').html("<label style='c-canceled'>Canceled</label>");
                    $('#settoarchive').text('Deceased details will be moved to Archives.');
                }
            });

            $('#editcrem_status').on('change', function(){
                var status = $(this).val();
                
                if(status == "Incomplete Requirements"){
                    $('#lblStatus').html("<label class='c-incomplete'>Incomplete Requirements</label>");
                    $('#settoarchive').text('');
                }
                else if(status == "Initially Scheduled"){
                    $('#lblStatus').html("<label class='c-initially: #342897;'>Initially Scheduled</label>");
                    $('#settoarchive').text('');
                }
                else if(status == "Schedule Confirmed"){
                    $('#lblStatus').html("<label style='c-confirmed;'>Schedule Confirmed</label>");
                    $('#settoarchive').text('');
                }
                else if(status == "Cremation Service Completed"){
                    $('#lblStatus').html("<label style='c-completed'>Cremation Service Completed</label>");
                    $('#settoarchive').text('Deceased details will be moved to Archives.');
                }
                else if(status == "Canceled"){
                    $('#lblStatus').html("<label style='c-canceled'>Canceled</label>");
                    $('#settoarchive').text('Deceased details will be moved to Archives.');
                }
            });

        // monitor readio button change
            $('#editscheduleform input[type="radio"]').on('change', function(){
                $('.dcase').val($('input[name=dcasecheck]:checked', '#editscheduleform').val());
                $('.dbuilt').val($('input[name=dbuiltcheck]:checked', '#editscheduleform').val());
                $('.dtype').val($('input[name=dtypecheck]:checked', '#editscheduleform').val());
                $('.dbonetype').val($('input[name=dbonetypecheck]:checked', '#editscheduleform').val());
                
            }) 

        // Function for Dates
            function setDate(month, day, year, dayclass, yearclass, dateclass){
                if(day != '' && month != '' && year != ''){
                    if(day.length == 1){
                        day = '0' + day;
                        $(dayclass).val(day);
                    }
                    var checkdate = moment(year+'-'+month+'-'+day, 'YYYY-MM-DD').isValid(); 
                    if(checkdate){
                        $(yearclass).removeClass('error'); $(dayclass).removeClass('error');
                        $(dateclass).val(year + '-' + month + '-' + day);
                    }
                    else{
                        var checkyear = moment(year, 'YYYY').isValid();
                        var checkday = moment(day, 'DD').isValid();
                        if(checkyear != true){
                            $(yearclass).addClass('error');
                        }
                        if(checkday != true){
                            $(dayclass).addClass('error');
                        }
                    }
                    return true;
                    }
                else{
                    return false;
                }
                return true;
            }
    </script>
@stop