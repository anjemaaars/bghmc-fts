@if( ( $template['page_title'] ) == 'Personnel') 

<div class="box boxer">
    <a href="{{ URL::route('display.IMPS') }}" >
        <div class="boxes" ><i class="fa fa-flag"></i>  FLAGGED <br><span class="btn"> {{$impCTR}}</span></div>
    </a>
    <a href="{{ URL::route('display.RECEIVED') }}">
        <div class="boxes" ><i class="fa fa-inbox"></i>  INBOX <br><span class="btn"> {{$recCTR}}</span></div>
    </a>
    <a href="{{ URL::route('display.SENT') }}">
        <div class="boxes" ><i class="fa fa-file-text-o"></i>  OUTBOX <br><span class="btn"> {{$UNRCTR}}</span></div>
    </a>
</div>

@elseif( ( $template['page_title'] ) == 'Administrator') 

<div class="box boxer">
    <a href="{{ URL::route('displayAdmin.IMPS') }}" >
        <div class="boxes" ><i class="fa fa-flag"></i>  FLAGGED <br><span class="btn"> {{$impCTR}}</span></div>
    </a>
    <a href="{{ URL::route('displayAdmin.RECEIVED') }}">
        <div class="boxes" ><i class="fa fa-inbox"></i>  INBOX <br><span class="btn"> {{$recCTR}}</span></div>
    </a>
    <a href="{{ URL::route('displayAdmin.SENT') }}">
        <div class="boxes" ><i class="fa fa-file-text-o"></i>  OUTBOX <br><span class="btn"> {{$UNRCTR}}</span></div>
    </a>
</div>

@else

@endif