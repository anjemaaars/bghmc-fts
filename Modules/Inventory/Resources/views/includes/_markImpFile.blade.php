@if(($per_files->_important)==0)
<a href="{{ URL::route('EveryFiles.show',$per_files->tracking_no) }}" class="btn btn-function">
    <div class="stuff">
        <i class="fa fa-flag-o">
            <span class="hoverstext">Mark as Important</span>    
        </i> 
    </div>
</a>
@elseif(($per_files->_important)==1)
<a href="{{ URL::route('EveryFiles.show',$per_files->tracking_no) }}" class="btn btn-function">
    <div class="stuff">
        <i class="fa fa-flag ">
            <span class="hoverstext">Mark as Not Important</span>
        </i>
    </div>
</a>
@else
    <a href="{{ URL::route('EveryFiles.show','ERROR') }}" class="btn btn-function">
        ERROR!
    </a>
@endif