@if (($tite_page)=='Home')        
    <i class="glyphicon glyphicon-home"></i> {{$tite_page}}
@elseif(($tite_page)=='All Files')
    <i class="glyphicon glyphicon-folder-open"></i> {{$tite_page}}
@elseif(($tite_page)=='Sent Files')
    <i class="glyphicon glyphicon-save-file"></i> {{$tite_page}}
@elseif(($tite_page)=='Received Files')
    <i class="glyphicon glyphicon-open-file"></i> {{$tite_page}}
@elseif(($tite_page)=='Draft Files')
    <i class="glyphicon glyphicon-file"></i> {{$tite_page}}
@elseif(($tite_page)=='Important Files')
    <i class="glyphicon glyphicon-pushpin"></i> {{$tite_page}}
@elseif(($tite_page)=='Bin')
    <i class="glyphicon glyphicon-trash"></i> {{$tite_page}}
@endif    