<form class="thing-form" file='true' type="form" enctype="multipart/form-data" name="newFile" id="newFile" method="post" action="{{ URL::route('Conversations.store') }}">
    {{ csrf_field() }}

    <div class="form-group has-feedback">
        
<!-- ____RECEIVER____ -->
        <div class="form-group">
            <label class="control-label float-left" for="receiver">Recepients
                <a class="nextStep" href="#modal-allUsers-file" data-toggle="modal" data-dismiss="modal">
                    <span class="fa fa-user-plus"></span> Add
                </a>&nbsp; &nbsp; &nbsp;
                <span id="Recepients"></span>
            </label>
            <div class="form-group form-group-pad">
                <select class="form-control form-control-pad" id="receiver2" onfocus="witwew2();" onchange="selectreceiver();">
                    <option>SELECT RECEIVER</option>
                    @foreach($emps as $emps)
                        <option value="{{$emps->emp_id}}" id="{{$emps->f_name}} {{$emps->l_name}}">{{$emps->f_name}} {{$emps->l_name}}</option>
                    @endforeach
                </select>
                <input id="searchRECEPIENT" type="text" name="searchRECEPIENT" class="form-control form-control-pad" placeholder="Search..." onfocus="witwew();" onclick ="searchthis();" onkeyup="asd();">    
            </div>

    <!-- ================ FOR SINGLE RECEIVER ============= -->
            <div class="col-md-8 col-md-offset-3" id=  "hideThis">
                <input id="receiver" type="text" class="form-control" name="receiver" value="{{ old('receiver') }}" placeholder="Recepients"  autofocus="autofocus">
            </div>
    <!-- ================ FOR MULTIPLE RECEIVERS ============ -->

        </div>
<!-- _____FILETYPE______ -->
        <div class="form-group">
            <label class="control-label float-left" for="filetype_id">File Type</label>
            <div class="form-group form-group-pad">
                <select class="form-control form-control-pad" id="filetype_id2" onchange="selectType();">
                    <option>SELECT FILE TYPE</option>
                    @foreach($ftypes as $ftypes)
                        <option value="{{$ftypes->filetype_name}}">{{$ftypes->filetype_name}}</option>
                    @endforeach
                </select> 
            </div>
            <div class="col-md-8 col-md-offset-3"  id="hideThis">
                <input id="filetype_id" type="text" class="form-control" name="filetype_id" value="{{ old('filetype_id') }}" placeholder="File Type"  autofocus="autofocus">
            </div>
        </div>
<!-- _____file_name_____ -->
        <div class="form-group">
            <label class="control-label float-left" for="file_name">File Name</label>
            <div class="form-group form-group-pad">
                <input type="text" class="form-control form-control-pad" name="file_name" placeholder="File Name">
            </div>
        </div>
<!-- ____CONTENT____ -->
        <div class="form-group">
            <label class="control-label float-left" for="fileText">Content</label>
            <div class="form-group form-group-pad">
                <textarea type="text" class="form-control form-control-pad textContent" name="fileText" placeholder="Your Message"></textarea>
            </div>
        </div>
<!-- _____URGENT FILE_____ -->
        <input type="text" name="Urgent" value="1" id="hideThis">
<!-- ____ATTACHMENT____ -->
        <div class="form-group">
            <label class="control-label float-left" for="attach">Attach File</label>
            <input type="file" name="fileToUpload" id="fileToUpload">
        </div>
    </div>
    <br>
    <div class="modal-footer">
        <button type="button" class="btn btn-lg " data-dismiss="modal">Cancel</button>
        <input type="submit" value="Send File" name="submit" class="btn btn-new btn-lg" >
    </div>
</form>