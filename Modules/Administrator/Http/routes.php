<?php

Route::group(['middleware' => ['web','auth'], 'prefix' => 'administrator', 'namespace' => 'Modules\Administrator\Http\Controllers'], function()
{
    //Navigations
    Route::get('/', 'AdministratorController@index')->name('admin.index');
    Route::get('/accounts', 'PersonnelController@index')->name('admin.accounts_list');
    Route::get('/departments', 'DeptController@index')->name('admin.departments_list');
    Route::get('/positions', 'PosController@index')->name('admin.positions_list');
    Route::get('/file-types', 'FilesController@index')->name('admin.filetypes_list');
    Route::get('/file-archives', 'ArchivesController@index')->name('admin.file_archives');
    Route::get('/file-report-generate', 'ReportController@index')->name('admin.file_generate');

    //Accounts
    Route::post('/register_account', array('uses' => 'PersonnelController@register_personnel'))->name('admin.registerpersonnel');
    Route::post('/update_account', array('uses' => 'PersonnelController@update_personnel'))->name('admin.updatepersonnel');
    Route::post('/update_activity', array('uses' => 'PersonnelController@update_activity'))->name('admin.updateactivity');
    Route::post('/update_password', array('uses' => 'AdministratorController@update_password'))->name('admin.updatepassword');
    Route::get('/account/{id}', array('uses' => 'PersonnelController@showAccount'))->name("personnel.show");

    //Departments
    Route::post('/new_department', array('uses' => 'DeptController@new_department'))->name('admin.newdepartment');
    Route::post('/update_department', array('uses' => 'DeptController@update_department'))->name('admin.updatedepartment');

    //Positions
    Route::post('/new_position', array('uses' => 'PosController@new_position'))->name('admin.newposition');
    Route::post('/update_position', array('uses' => 'PosController@update_position'))->name('admin.updateposition');

    //File Types
    Route::post('/new_filetype', array('uses' => 'FilesController@new_filetype'))->name('admin.newfiletype');
    Route::post('/update_filetype', array('uses' => 'FilesController@update_filetype'))->name('admin.updatefiletype');

    Route::get('/password/change', 'PersonnelController@password_change')->name('accnt.passchange');
    Route::post('/password/change_post', 'PersonnelController@password_change_post')->name('accnt.passchangepost');

    
});