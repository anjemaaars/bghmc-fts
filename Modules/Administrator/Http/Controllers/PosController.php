<?php

namespace Modules\Administrator\Http\Controllers;

use Modules\Setup\Init;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;

use Modules\Administrator\Entities\PositionsModel as PosM;
use Modules\Administrator\Entities\SystemLogsModel as SLM;

class PosController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    protected $data;
    protected $page_title = 'Positions';

    function setup($vars = null)
    {
        $Init = new Init;
        $vars['page'] = $this->page_title;
        $this->data['template'] = $Init->setup($vars);
        $logs = new SLM;
        $this->data['logs'] = $logs->show_logs();
        return $this->data;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $pos_info = DB::table('bghmc_positions')->get(); 
        $this->data['info'] = $pos_info;

        return view('administrator::positions_index', $this->setup());
    }

    public function new_position(Request $request){
        $data = $request->all();
        
        $is_existing = DB::table('bghmc_positions')->WHERE(strtolower('pos_name'), strtolower($request->input('posname')))->first();
        
        $PosM = new PosM;
        if($PosM->validate($data, ''))
        {
            if($is_existing){ 
                $data['status'] = 0;
                $data['errors']['message'] = "Position Already Registered";
            }
            else{
                $PosM->setInfo($request);
                $data['status'] = 1;
                $data['errors']['message'] = 'Position successfully added';
            }
        }
        else
        {
            $data['status'] = 0;
            $data['errors'] = $PosM->errors();
        }
        // Return to modal if success or fail
        return $data;
    }

    public function update_position(Request $request){
        $data = $request->all();
        
        $PosM = new PosM; 
        
        if($PosM->validate($data, '')) 
        {
            $PosM->updateInfo($request); 
            $data['status'] = 1;
            $data['errors']['message'] = 'Position successfully updated';
        }
        else
        {
            $data['status'] = 0;
            $data['errors'] = $PosM->errors();
        }
        // Return to modal if success or fail
        return $data;
    }
}
