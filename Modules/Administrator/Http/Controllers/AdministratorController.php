<?php

namespace Modules\Administrator\Http\Controllers;

use Modules\Setup\Init;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;

use Illuminate\Support\Facades\Hash;
use Modules\Template\Entities\UserGroup;
use Modules\Template\Entities\UserCredentials;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Session;
use Modules\Administrator\Entities\SystemLogsModel as SLM;



class AdministratorController extends Controller
{
      /**
     * Display a listing of the resource.
     * @return Response
     */
    protected $data;
    protected $page_title = 'Login';

    function setup($vars = null)
    {
        $Init = new Init;
        $vars['page'] = $this->page_title;
        $this->data['template'] = $Init->setup($vars);
        $logs = new SLM;
        $this->data['logs'] = $logs->show_logs();
        return $this->data;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(){
        $emp_info = DB::table('bghmc_employee_info')->WHERE('emp_id', Session::get('bghmcuser')->emp_id)->first();
        if(password_verify(1, $emp_info->password)){
            // $this->data['empid'] = $emp_info->emp_id;
            // return view('template::top-nav-pages.change_default_pass',$this->setup());
            return redirect()->route('accnt.passchange');
        }else{
            return view('administrator::administrator.index',$this->setup());
        }
    }
}
