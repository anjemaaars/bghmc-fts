<?php

namespace Modules\Administrator\Http\Controllers;

use Modules\Setup\Init;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Administrator\Entities\SystemLogsModel as SLM;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    protected $data;
    protected $page_title = 'Generate Report';

    function setup($vars = null)
    {
        $Init = new Init;
        $vars['page'] = $this->page_title;
        $this->data['template'] = $Init->setup($vars);
        $logs = new SLM;
        $this->data['logs'] = $logs->show_logs();
        return $this->data;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(){
        // $dept_info = DB::table('filetypes')->get(); 
        // $this->data['info'] = $dept_info;

        return view('administrator::file.file_generate',$this->setup());
    }
}
