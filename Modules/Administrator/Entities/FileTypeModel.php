<?php

namespace Modules\Administrator\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

use Modules\Administrator\Entities\FileTypeModel as FTM;
use Modules\Administrator\Entities\SystemLogsModel as SLM;

class FileTypeModel extends BaseModel
{
    protected $table = 'bghmc_filetypes';
    protected $fillable = ['filetype_id', 'filetype_name'];
    protected $primaryKey = 'filetype_id';

    protected $rules = array(
        'filetype' => 'Required|min:3'
    );

    public function setInfo($request){
        $count = DB::table('bghmc_filetypes')->get();
        
        $FTM = new FTM;
        $FTM->filetype_id = 70001 + $count->count();
        $FTM->filetype_name = $request->input('filetype');
        $FTM->save();

        $L = new SLM;
        $L->setLog($request, '', 'new_filetype', null);
    }

    public function updateInfo($request){
        $FTMinfo = FTM::find($request->input('filetypeid'));
        $FTM = FTM::find($request->input('filetypeid'));
        $FTM->filetype_name = $request->input('filetype');
        $FTM->save();

        $L = new SLM;
        $L->setLog($request, $FTMinfo, 'update_filetype', null);
    }
}
