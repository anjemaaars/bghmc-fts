<?php

namespace Modules\Administrator\Entities;

use Illuminate\Database\Eloquent\Model;
use Validator;


class baseModel extends Model
{
    protected $fillable = [];
    protected $table = '';
    protected $rules = array();

    protected $errors;

    public function validate($data, $whatdata)
    {
        if($whatdata == 'register_personnel'){
            $rules = array(
                'empid' => 'Required|min:3|regex:/^[0-9]+$/',
                'fname' => 'Required',
                'lname' => 'Required',
                'rd-accnttype' => 'Required',
                'drp-pos' => 'Required',
                'drp-dept' => 'Required',
            );
        }
        else if($whatdata == 'update_personnel'){
            $rules = array(
                // 'password' => 'Required|min:3',
                'fname' => 'Required',
                'lname' => 'Required',
                'drp-pos' => 'Required',
                'drp-dept' => 'Required',
            );
        }
        else if($whatdata == "change_password"){
            $rules = array(
                'password' => 'Required|min:5',
                'repeat_password' => 'Required|same:password|min:5'
            );
        }
        else if($whatdata == "deactivate"){
            $rules = array(
                'reason' => 'Required|min:5'
            );
        }

        if($whatdata != ''){$v = Validator::make($data, $rules);}
        else{$v = Validator::make($data, $this->rules);}
        
        if($v->fails())
        {
            $this->errors = $v->messages();
            return false;
        }
        else{
            return true;
        }
    }

    public function errors()
    {
        return $this->errors;
    }
}
