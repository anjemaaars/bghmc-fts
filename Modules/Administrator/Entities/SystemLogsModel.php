<?php

namespace Modules\Administrator\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Carbon;
use Modules\Administrator\Entities\SystemLogsModel as SLM;

class SystemLogsModel extends BaseModel
{
    protected $table = 'bghmc_system_logs';
    protected $fillable = ['logged_user', 'log', 'log_data_before', 'log_data_after', 'type'];

    public function setLog($data, $oldinfo, $logtype, $accnt_activity){
        $this->logged_user = Session::get('bghmcuser')->emp_id;
        if($logtype == 'new_account'){
            $this->log = 'New account "' . $data->input('fname') . ' ' . $data->input('lname') . '" with ID #' . $data->input('empid') . ' added.';
        }
        else if($logtype == 'update_account'){
            $this->log = 'Account of "' . $oldinfo->f_name . ' ' . $oldinfo->l_name . '" with ID ' . $oldinfo->emp_id . ' updated.';
        }
        else if($logtype == 'update_activity'){
            $this->log = 'Account of "' . $oldinfo->f_name . ' ' . $oldinfo->l_name . '" with ID ' . $oldinfo->emp_id . ' ' . $data->act .'d.';
            $oldinfo = serialize($accnt_activity);
        }
        else if($logtype == 'new_department'){
            $this->log = 'New department "' . $data->input('deptname') . '" added.';
        }
        else if($logtype == 'update_department'){
            $this->log = 'Department "' . $oldinfo->dept_name . '" updated.';
        }
        else if($logtype == 'new_position'){
            $this->log = 'New position "' . $data->input('posname') . '" added.';
        }
        else if($logtype == 'update_position'){
            $this->log = 'Position "' . $oldinfo->pos_name . '" updated.';
        }
        else if($logtype == 'new_filetype'){
            $this->log = 'New file type "' . $data->input('filetype') . '" added.';
        }
        else if($logtype == 'update_filetype'){
            $this->log = 'File type "' . $oldinfo->filetype_name . '" updated.';
        }
        $this->log_data_before = $oldinfo;
        $this->log_data_after = serialize($data->all());
        $this->type = $logtype;
        $this->save();
    }

    public function show_logs(){
        $loginfo = DB::table('bghmc_system_logs')->WHERE('logged_user', Session::get('bghmcuser')->emp_id)->where('created_at', '>=', \Carbon::today()->toDateString())->orderBy('created_at', 'desc')->get();
        // $this->data['logs'] = $loginfo;
        return $loginfo;
        // return view('template::admin-layouts.includes.controler-sidebar', compact('logs',));
    }
}
