<?php

namespace Modules\Administrator\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

use Modules\Administrator\Entities\PositionsModel as PosM;
use Modules\Administrator\Entities\SystemLogsModel as SLM;

class PositionsModel extends BaseModel
{
    protected $table = 'bghmc_positions';
    protected $fillable = ['pos_id', 'pos_name'];
    protected $primaryKey = 'pos_id';
    
    protected $rules = array(
        'posname' => 'Required|min:3'
    );
    
    public function setInfo($request){
        $count = DB::table('bghmc_positions')->get();
            
        $PosM = new PosM;
        $PosM->pos_id = 80001 + $count->count();
        $PosM->pos_name = $request->input('posname');
        $PosM->save();

        $L = new SLM;
        $L->setLog($request, '', 'new_position', null);
    }
    public function updateInfo($request){
        $PosMinfo = PosM::find($request->input('posid'));
        $PosM = PosM::find($request->input('posid'));
        $PosM->pos_name = $request->input('posname');
        $PosM->save();

        $L = new SLM;
        $L->setLog($request, $PosMinfo, 'update_position', null);
    }
}
