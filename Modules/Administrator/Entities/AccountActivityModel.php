<?php

namespace Modules\Administrator\Entities;

use Illuminate\Database\Eloquent\Model;

class AccountActivityModel extends BaseModel
{
    protected $table = 'bghmc_account_activity';
    protected $fillable = ['emp_id', 'deact_reason', 'isactive'];
    // protected $primaryKey = 'emp_id';
}
