 @extends('template::admin-pages.menus.'.$template['menu']) 

@section('plugins-css')
    <link rel="stylesheet" href="{{asset('adminlte')}}/plugins/datatables/dataTables.bootstrap.css">
@stop

@section('pagename')
    <span style="font-size: 25px;">Accounts</span>
@stop

@section('content')
    <div class="content-wrapper">
        <section class="content">
        <!-- {{$hash}} -->
            <div class="box">
                <div class="box-header">
                    <button type="button" class="btn btn-new btn-lg pull-right" data-toggle="modal" data-target="#mdl-new-personnel">REGISTER ACCOUNT&nbsp;&nbsp;&nbsp;
                    <i class="fa fa-user-plus"></i> </button>
                </div>
                <div class="box-body">
                    <table id="tbl_personnel" class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <td>Employee ID</td>
                                <td>First Name</td>
                                <td>Last Name</td>
                                <td>Position</td>
                                <td>Department</td>
                                <td>Account Created</td>
                                <td>Is Active?</td>
                                <td class="hidden-xs"></td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($info as $i)
                            <tr>
                                <td>{{$i->emp_id}}</td>
                                <td>{{$i->f_name}}</td>
                                <td>{{$i->l_name}}</td>
                                <td>{{$i->pos_id}}</td>
                                <td>{{$i->dept_id}}</td>
                                <td>{{$i->created_at}}</td>
                                <td>
                                    @if($i->isactive == 1)
                                        <span style="color: green;"><i class="glyphicon glyphicon-ok"></i></span>
                                    @else
                                        <span style="color: red;"><i class="glyphicon glyphicon-remove"></i></span>
                                    @endif
                                </td>
                                <td class="hiddex-xs">
                                <a href="{{ route('personnel.show',array('id'=>$i->emp_id)) }}" class="btn btn-function btn-edit"><i class="glyphicon glyphicon-eye-open"></i>&nbsp;&nbsp; View</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
    </div>

<!--MODAL NEW PERSONNEL-->
  <div id="mdl-new-personnel" class="modal fade table-header">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <div class="text-center">
                    <span class="title">Register Personnel Account</span>
                </div>
            </div>
            <div class="modal-body form-group-pad-body">
                <div id="status"></div>
                    <form class="thing-form" role="form" name="newpersonnelform" id="newpersonnelform" method="post" >
                    {{ csrf_field() }}
                        <div class="form-group has-feedback">
                                <div class="form-group">
                                    <label class="control-label float-left" for="empid">Employee ID</label>
                                    <div class="form-group form-group-pad">
                                        <input type="text" class="form-control form-control-pad" name="empid" placeholder="#"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label float-left" for="password">Default Password (To be changed once newly logged in): </label>1
                                </div>
                                <div class="form-group">
                                    <label class="control-label float-left" for="name">Name</label>
                                    <div class="form-group form-group-pad">
                                        <input type="text" class="form-control form-control-pad" name="fname" placeholder="First name"/>
                                        <input type="text" class="form-control form-control-pad" name="lname" placeholder="Last name"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label float-left" for="accnttype">Account Type</label>
                                    <div class="form-group form-group-pad">
                                        <input type="radio" class="form-control-pad" name="rd-accnttype" value="administrator"/> Administrator
                                        <input type="radio" class="form-control-pad" name="rd-accnttype" value="personnel" style="margin-left: 10px;"/> Personnel
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label float-left" for="position">Position</label>
                                    <div class="form-group form-group-pad">
                                        <select name="drp-pos" class="form-control-pad form-control">
                                            <option selected value="">Choose Position</option>
                                            @foreach ($pos as $p)
                                                <option value="{{$p->pos_name}}">{{$p->pos_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label float-left" for="department">Department</label>
                                    <div class="form-group form-group-pad">
                                        <select name="drp-dept" class="form-control-pad form-control">
                                            <option selected value="0">Choose Department</option>
                                            @foreach ($depts as $d)
                                                <option value="{{$d->dept_name}}">{{$d->dept_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-md-9" style="text-align: right;"><a class="btn btn-lg" data-dismiss="modal">Cancel</a></div>
                            <div class="col-md-3">
                                <button type="button" class="btn btn-block btn-new btn-lg" onclick="$(this).sendPersonnelInfo('{{route('admin.registerpersonnel')}}', '#newpersonnelform');">Register</button>
                            </div>
                        </div>
                    </form>
            </div>
        </div>
    </div>
  </div>
@stop

@section('plugins-script')
    <script src="{{asset('adminlte')}}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{{asset('adminlte')}}/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script>
        $('#tbl_personnel').DataTable();
        $('.modal').on('hidden.bs.modal', function () {
            $(".modal-body #status").html('<div></div>');
            $('#newpersonnelform').clearForm();
        });
        $.fn.clearForm = function() {
            return this.each(function() {
              var type = this.type, tag = this.tagName.toLowerCase();
              if (tag == 'form')
                return $(':input',this).clearForm();
              if (type == 'text' || type == 'password' || tag == 'textarea')
                this.value = '';
              else if (type == 'checkbox' || type == 'radio')
                this.checked = false;
              else if (tag == 'select')
                this.selectedIndex = -1;
            });
          };
        $.fn.sendPersonnelInfo = function(rt,fr,willreload,mod){
            $.ajax({
                type : 'POST',
                url : rt, //from routes
                data: $(fr).serialize(),
                // dataType : 'json',
                error : function(){
                    alert('error');
                },
                success : function(data){
                    var errors = '';
                    if(data['status']==0){
                        for(var key in data['errors']){
                            errors += data['errors'][key]+'<br />';
                        }
                        $(' .modal-body  #status').html('<div class="alert alert-danger alert-dismissible">'+errors+'</div>').fadeIn();
                    }else{
                        $(' .modal-body  #status').html('<div class="alert alert-success alert-dismissible"><h4><i class="icon fa fa-ban"></i>'+data['errors']['message']+'</h4></div>').fadeIn().delay(1500).fadeOut(1000);
                        location.reload();
                    }
            
                }
            });
        };
    </script>
@stop