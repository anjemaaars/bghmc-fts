@extends('template::admin-pages.menus.'.$template['menu']) 

@section('plugins-css')
    <link rel="stylesheet" href="{{asset('adminlte')}}/plugins/datatables/dataTables.bootstrap.css">
@stop

@section('pagename')
    <span style="font-size: 25px;">Accounts</span>
@stop

@section('content')
    <div class="content-wrapper">
        <section class="content">
            <div class="pull-left" style="padding: 10px;">
                <a href="{{ route($backroute, null)}}" style="color: #000; font-size: 20px;"><i class="glyphicon glyphicon-chevron-left"></i></a>
            </div>
            <div class="pull-left" style="font-size: 30px; padding-left: 10px; margin-bottom: 5px;">
                <span style="font-size: 20px;">{{$info->f_name}} {{$info->l_name}} ( ID #{{$info->emp_id}} )</span>
            </div>
            <div style="margin-top: 60px;"></div>
            <div class="row">
                <div class="col-sm-4">
                <!--///////// ACCOUNT-->
                    <div class="box box-primary">
                        <div class="body-box">
                            <div class="container">
                                <div style="padding: 10px;">
                                    <b class="text-bts">ACCOUNT INFORMATION</b><button type="button" class="btn btn-function" data-toggle="modal" data-target="#mdl-edit-personnel"><i class="glyphicon glyphicon-pencil"></i></button>
                                </div>
                                <table class="table">
                                    <tr>
                                        <td style="width: 100px; text-align: right;">
                                            <i style="color: {{$info->isactive == 1 ? 'green':'red'}};" class="glyphicon glyphicon-{{$info->isactive == 1 ? 'ok':'remove'}}"></i>
                                        </td>
                                        <td><b>Account is {{ ($info->isactive == 1 ? "active":"deactivated") }}</b></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right;"><label>Name:</label></td>
                                        <td><label>{{$info->l_name}}, {{$info->f_name}}</label></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right;"><label>Account:</label></td>
                                        <td>{{$info->accnt_type}}</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right;"><label>Position:</label></td>
                                        <td>{{$info->pos_id}}</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right;"><label>Department:</label></td>
                                        <td>{{$info->dept_id}}</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right;"><label>Date Created:</label></td>
                                        <td>{{\Carbon\Carbon::parse($info->created_at)->format('M d Y h:m A')}}</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right;"><label>Last Updated:</label></td>
                                        <td>{{\Carbon\Carbon::parse($info->updated_at)->format('M d Y h:m A')}}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-8">
                <!--///////// ACTIVITY-->
                    <div class="box box-primary">
                            <div class="body-box">
                                <div class="container">
                                    <div style="padding: 10px;">
                                        <b class="text-bts">ACCOUNT ACTIVATION / DEACTIVATION HISTORY</b>
                                    </div>
                                    <table class="table table-striped table-bordered table-hover" id="tbl_activity">
                                        <thead>
                                            <tr>
                                                <td></td>
                                                <td>Activity Type</td>
                                                <td>Reason / Description</td>
                                                <td>Date</td>
                                                <td>Time</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($act as $a)
                                            <tr>
                                                <td>{{$index++}}</td>
                                                <td style="color:{{$a->isactive == 1 ? 'green':'red'}};">{{$a->isactive == 1 ? 'Activate':'Deactivate'}}d</td>
                                                <td>{{$a->deact_reason}}</td>
                                                <td>{{\Carbon\Carbon::parse($a->created_at)->format('M d Y')}}</td>
                                                <td>{{\Carbon\Carbon::parse($a->created_at)->format('h:i A')}}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!--Modal to update details  -->
    <div id="mdl-edit-personnel" class="modal fade table-header mdl-arch">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <div class="text-center">
                        <span class="title">Update Account of {{$info->f_name}} {{$info->l_name}}</span>
                    </div>
                </div>
                <div class="modal-body form-group-pad-body">
                    <center>
                        <b>Account is {{ ($info->isactive == 1 ? "active":"deactivated") }}</b><br><br>
                        <a href="" class="btn btn-function btn-activity" data-dismiss="modal" data-toggle="modal" data-target="#mdl-activity-personnel">{{ ($info->isactive == 1 ? 'deactivate':'activate') }}</a>
                    </center>
                    <hr>
                    <div id="status"></div>
                    <form class="thing-form" role="form" name="editpersonnelform" id="editpersonnelform" method="post">
                        {{ csrf_field() }}
                        <input type="hidden" name="e_id" value="{{$info->emp_id}}" />
                        <input type="hidden" name="accnt_type" value="{{$info->accnt_type}}" />
                        <div class="form-group has-feedback">
                            <div class="form-group">
                                <label class="control-label float-left" for="name">Name</label>
                                <div class="form-group form-group-pad">
                                    <input type="text" class="form-control form-control-pad" name="fname" placeholder="First name" value="{{$info->f_name}}" />
                                    <input type="text" class="form-control form-control-pad" name="lname" placeholder="Last name" value="{{$info->l_name}}" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label float-left" for="position">Position</label>
                                <div class="form-group form-group-pad">
                                    <select name="drp-pos" class="form-control-pad form-control">
                                        <?php $posid = $iposid;?>
                                        @foreach ($pos as $p)
                                        <?php $posidp = $p->pos_name; ?>
                                        <option value="{{$p->pos_name}}" {{ ($posid== $posidp ? "selected": "") }}>{{$p->pos_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label float-left" for="department">Department</label>
                                <div class="form-group form-group-pad">
                                    <select name="drp-dept" class="form-control-pad form-control">
                                        <?php $deptid = $ideptid;?> 
                                        @foreach ($depts as $d)
                                        <?php $deptidp = $d->dept_name; ?>
                                        <option value="{{$d->dept_name}}" {{ ($deptid== $deptidp ? "selected": "") }}>{{$d->dept_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-md-9" style="text-align: right;"><a class="btn btn-lg" data-dismiss="modal">Cancel</a></div>
                            <div class="col-md-3">
                                <button type="button" class="btn btn-block btn-new btn-lg" onclick="$(this).sendPersonnelInfo('{{route('admin.updatepersonnel')}}', '#editpersonnelform');">Update</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!--Modal to activate/deactivate  -->
    <div id="mdl-activity-personnel" class="modal fade table-header mdl-arch">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <div class="text-center">
                        @if($info->isactive == '1')
                        <span class="title"><span style="color: red;">Deactivate</span> Account of {{$info->f_name}} {{$info->l_name}}</span>
                        @else
                        <span class="title"><span style="color: green;">Activate</span> Account of {{$info->f_name}} {{$info->l_name}}</span>
                        @endif
                    </div>
                </div>
                <div class="modal-body form-group-pad-body row">
                    <div id="status"></div>
                    <form class="thing-form" role="form" name="updateactivityform" id="updateactivityform" method="post">
                        {{ csrf_field() }}
                        <input type="hidden" name="e_id" value="{{$info->emp_id}}">
                        <p class="text-center">Are you sure to {{ ($info->isactive == 1 ? 'deactivate':'activate') }} the following account?</p>

                        <div class="col-sm-12">
                            <div class="col-sm-6 text-right">Employee ID:</div>
                            <div class="col-sm-6"><b>{{$info->emp_id}}</b></div>
                        </div>
                        <div class="col-sm-12">
                            <div class="col-sm-6 text-right">Name:</div>
                            <div class="col-sm-6"><b>{{$info->f_name}} {{$info->l_name}}</b></div>
                        </div>
                        <div class="col-sm-12">
                            <div class="col-sm-6 text-right">Position:</div>
                            <div class="col-sm-6"><b>{{$info->pos_id}}</b></div>
                        </div>
                        <div class="col-sm-12">
                            <div class="col-sm-6 text-right">Department:</div>
                            <div class="col-sm-6"><b>{{$info->dept_id}}</b></div>
                        </div>
                        @if($info->isactive == 1)
                        <div class="col-sm-12">
                            <hr>
                            <div class="form-group has-feedback">
                                <div class="form-group">
                                    <label class="control-label text-center" for="reason">State reason for deactivation</label>
                                    <div class="form-group form-group-pad">
                                        <textarea class="form-control" name="reason"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                        <div style="margin-top: 20px;" class="col-sm-12">
                            <div class="col-sm-6"><a class="btn btn-function btn-cancel" data-dismiss="modal" data-toggle="modal" data-target="#mdl-edit-personnel">Cancel</a></div>
                            <div class="col-sm-6">
                                <input type="hidden" name="act" value="{{ ($info->isactive == 1 ? 'deactivate':'activate') }}">
                                <button type="button" class="btn btn-function btna-{{ ($info->isactive == 1 ? 'deactivate':'activate') }}" style="width:100%;"
                                    onclick="$(this).sendPersonnelInfo('{{route('admin.updateactivity')}}', '#updateactivityform');">{{ ($info->isactive == 1 ? 'deactivate':'activate') }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop
@section('plugins-script')
    <script src="{{asset('adminlte')}}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{{asset('adminlte')}}/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script>
        $('#tbl_activity').DataTable();
        $.fn.sendPersonnelInfo = function(rt,fr,willreload,mod){
            $.ajax({
                type : 'POST',
                url : rt, //from routes
                data: $(fr).serialize(),
                // dataType : 'json',
                error : function(){
                    alert('error');
                },
                success : function(data){
                    var errors = '';
                    if(data['status']==0){
                        for(var key in data['errors']){
                            errors += data['errors'][key]+'<br />';
                        }
                        $(' .modal-body  #status').html('<div class="alert alert-danger alert-dismissible">'+errors+'</div>').fadeIn();
                    }else{
                        $(' .modal-body  #status').html('<div class="alert alert-success alert-dismissible"><h4><i class="icon fa fa-ban"></i>'+data['errors']['message']+'</h4></div>').fadeIn().delay(1500).fadeOut(1000);
                        location.reload();
                    }
            
                }
            });
        };
    </script>
@stop