 @extends('template::admin-pages.menus.'.$template['menu']) 

@section('plugins-css')
    <link rel="stylesheet" href="{{asset('adminlte')}}/plugins/datatables/dataTables.bootstrap.css">
@stop

@section('pagename')
    <span style="font-size: 25px;">Positions Summary</span>
@stop

@section('content')
    <div class="content-wrapper">
        <section class="content">
            <div class="box">
                <div class="box-header">
                    <button type="button" class="btn btn-new btn-lg pull-right" data-toggle="modal" data-target="#mdl-new-pos">NEW POSITION&nbsp;&nbsp;&nbsp;
                    <i class="fa fa-user-plus"></i> </button>
                </div>
                <div class="box-body">
                    <table id="tbl_positions" class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <td>Position ID</td>
                                <td>Position Name</td>
                                <td class="hidden-xs"></td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($info as $i)
                            <tr>
                                <td>{{$i->pos_id}}</td>
                                <td>{{$i->pos_name}}</td>
                                <td class="hiddex-xs">
                                    <a href="" class="btn btn-function btn-edit" data-toggle="modal" data-target="#mdl-edit-pos_{{$i->pos_id}}"><i class="glyphicon glyphicon-pencil"></i>&nbsp;&nbsp; Update</a>

                                <!--Modal to edit department  -->
                                <div id="mdl-edit-pos_{{$i->pos_id}}" class="modal fade table-header mdl-arch">
                                    <div class="modal-dialog modal-md">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                <div class="text-center">
                                                    <span class="title">Update Position</span>
                                                </div>
                                            </div>
                                            <div class="modal-body form-group-pad-body">
                                                <div id="status"></div>
                                                    <form class="thing-form" role="form" name="editpositionform_{{$i->pos_id}}" id="editpositionform_{{$i->pos_id}}" method="post" >
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="posid" value="{{$i->pos_id}}">
                                                    <center>
                                                        <div class="form-group has-feedback">
                                                            <div class="form-group">
                                                                <label class="control-label float-left" for="posname">Position Name</label>
                                                                <div class="form-group form-group-pad">
                                                                    <input type="text" class="form-control form-control-pad" name="posname" value="{{$i->pos_name}}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </center>
                                                        <br>
                                                        <div class="row">
                                                            <div class="col-md-9" style="text-align: right;"><a class="btn btn-lg" data-dismiss="modal">Cancel</a></div>
                                                            <div class="col-md-3">
                                                                <button type="button" class="btn btn-block btn-new btn-lg" onclick="$(this).sendPositionInfo('{{route('admin.updateposition')}}', '#editpositionform_{{$i->pos_id}}');">Update</button>
                                                            </div>
                                                        </div>
                                                    </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
    </div>

<!--MODAL NEW PERSONNEL-->
  <div id="mdl-new-pos" class="modal fade table-header">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <div class="text-center">
                    <span class="title">New Position</span>
                </div>
            </div>
            <div class="modal-body form-group-pad-body">
                <div id="status">
                </div>
                    <form class="thing-form" role="form" name="newpositionform" id="newpositionform" method="post" >
                    {{ csrf_field() }}
                        <div class="form-group has-feedback">
                            <div class="form-group">
                                <label class="control-label float-left" for="posname">Position</label>
                                <div class="form-group form-group-pad">
                                    <input type="text" class="form-control form-control-pad" name="posname">
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-9" style="text-align: right;"><a class="btn btn-lg" data-dismiss="modal">Cancel</a></div>
                            <div class="col-md-3">
                                <button type="button" class="btn btn-block btn-new btn-lg" onclick="$(this).sendPositionInfo('{{route('admin.newposition')}}', '#newpositionform');">Add</button>
                            </div>
                        </div>
                    </form>
            </div>
        </div>
    </div>
  </div>
@stop

@section('plugins-script')
    <script src="{{asset('adminlte')}}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{{asset('adminlte')}}/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script>
        $('#tbl_positions').DataTable();
        $('.modal').on('hidden.bs.modal', function () {
            $(".modal-body #status").html('<div></div>');
            $('#newpositionform').clearForm();
        });
        $.fn.clearForm = function() {
            return this.each(function() {
              var type = this.type, tag = this.tagName.toLowerCase();
              if (tag == 'form')
                return $(':input',this).clearForm();
              if (type == 'text' || type == 'password' || tag == 'textarea')
                this.value = '';
              else if (type == 'checkbox' || type == 'radio')
                this.checked = false;
              else if (tag == 'select')
                this.selectedIndex = -1;
            });
          };
        $.fn.sendPositionInfo = function(rt,fr){
            $.ajax({
                type : 'POST',
                url : rt, //from routes
                data: $(fr).serialize(),
                // dataType : 'json',
                error : function(){
                    alert('error');
                },
                success : function(data){
                    var errors = '';
                    if(data['status']==0){
                        for(var key in data['errors']){
                            errors += data['errors'][key]+'<br />';
                        }
                        $(' .modal-body  #status').html('<div class="alert alert-danger alert-dismissible">'+errors+'</div>').fadeIn();
                    }else{
                        $(' .modal-body  #status').html('<div class="alert alert-success alert-dismissible"><h4><i class="icon fa fa-ban"></i>'+data['errors']['message']+'</h4></div>').fadeIn().delay(1500).fadeOut(1000);
                        location.reload();
                    }
            
                }
            });
        };
    </script>
@stop