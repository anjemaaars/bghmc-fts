<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EmployeeInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bghmc_employee_info', function (Blueprint $table) {
            $table->engine = 'InnoDB ROW_FORMAT=DYNAMIC';

            $table->primary('emp_id');

            $table->integer('emp_id');
            $table->string('password',80);
            $table->string('f_name',80);
            $table->string('l_name',80); //->nullable();
            $table->string('accnt_type', 20);
            $table->integer('pos_id');
            $table->integer('dept_id');
            $table->boolean('isactive');
                    
            $table->timestamps();
            $table->softDeletes();

            $table->index(['accnt_type', 'l_name', 'isactive'],'bghmc_employee_info'); //keywords for db para mabilis mahanap
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bghmc_employee_info');
    }
}
