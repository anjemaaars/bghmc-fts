<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Positions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bghmc_positions', function (Blueprint $table) {
            $table->engine = 'InnoDB ROW_FORMAT=DYNAMIC';

            $table->primary('pos_id');

            $table->integer('pos_id');
            $table->string('pos_name', 80);
                    
            $table->timestamps();
            $table->softDeletes();

            $table->index(['pos_name'],'bghmc_positions'); //keywords for db para mabilis mahanap
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bghmc_positions');
    }
}
