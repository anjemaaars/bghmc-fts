<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Filetypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bghmc_filetypes', function (Blueprint $table) {
            $table->engine = 'InnoDB ROW_FORMAT=DYNAMIC';

            $table->primary('filetype_id');

            $table->integer('filetype_id');
            $table->string('filetype_name',250);
                    
            $table->timestamps();
            $table->softDeletes();

            // $table->index(['dept_name'],'departments'); //keywords for db para mabilis mahanap
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bghmc_filetypes');
    }
}
