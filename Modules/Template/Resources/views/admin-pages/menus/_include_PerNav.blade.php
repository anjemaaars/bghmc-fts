<!-- ___________________________________SIDE NAVIGATION ADDED FOR PERSONNELS__________________________ -->
@if( ( $template['page_title'] ) == 'Personnel') 

<li class="tabStuff" onmouseover="categoriesss(event, 'categs')">
  <a href="">
    <i class="glyphicon glyphicon-envelope"></i>
    Files
  </a>

</li>  
    <div id="categs" class="tabContentss" onmouseover="categoriesss(event, 'categs');" onmouseout="outcateg();">
        <ul>  
            <a href="{{ URL::route('EveryFiles.show','ALL_FILES') }}"><li><i class=" glyphicon glyphicon-folder-open"></i>  All My Files</li></a>
            <a href="/inventory/SENT_FILES"><li><i class=" glyphicon glyphicon-save-file"></i>  Sent Files</li></a>
            <a href="{{ URL::route('display.SENT') }}"><li><i class=" glyphicon glyphicon-open-file"></i>  Received Files</li></a>
            <a href="{{ URL::route('EveryFiles.show','IMPORTANT_FILES') }}"><li><i class=" glyphicon glyphicon-pushpin"></i>  Important Files</li></a>
            <a href="{{ URL::route('EveryFiles.show','DRAFT_FILES') }}"><li><i class=" glyphicon glyphicon-file"></i>  Drafts</li></a>
            <a href=""><li><i class=" glyphicon glyphicon-trash"></i>  Bin</li></a>
        </ul>
    </div>
@else
@endif