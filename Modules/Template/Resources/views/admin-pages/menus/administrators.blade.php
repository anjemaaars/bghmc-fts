@extends('template::admin-layouts.default')

@section('left-sidebar-menu')
<!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar" style="background-color:#f9fafc;">
      <div style="padding: 10px; text-align: center;">
        <b>Baguio General Hospital and Medical Center<br> File Tracking System</b>
      </div>
      <div class="user-panel">
        <div class="pull-left info">
          <p>ID # {{$template['realname']}} ( {{$template['page_title']}} )</p>
          <a href="{{url('')}}/logout" style="font-size: 13px;">
            <b><i class="glyphicon glyphicon-log-out" style="color: red;"></i>&nbsp;&nbsp;Sign out</b>
          </a>
        </div>
      </div>
      
      <hr style="margin: 10px;">
     <ul class="sidebar-menu">
        <!-- <li class="header"> -->
        <!-- ________________TRIP KO LANG HAHHAHHA__________________ -->
          <!-- @if( ( $template['page_title'] ) == 'Personnel') 
            MY NAVIGATIONS (=^_^=)
            
          @else 
            MAIN NAVIGATION
          @endif</li> -->

        @foreach($template['navs']['main'] as $nav)
            <?php
              $props_main = json_decode($nav->properties,true);

                   foreach ($props_main as $key => $value) {
                    $prop_main[$key] = '';
                            foreach ($props_main[$key] as $keyx => $valuex) {
                                $prop_main[$key] .= " $keyx=\"$valuex\" ";
                      }
                  }
            ?>
          @if($nav->parent==0)
              <li <?=array_has($prop_main, 'li')?$prop_main['li']:''?> >
                    <a href="{{Route::has($nav->route)? route($nav->route) : 'error'}}" <?=array_has($prop_main, 'a')?$prop_main['a']:''?> >
                        <i <?=array_has($prop_main, 'i')?$prop_main['i']:''?> ></i>
                        {{$nav->title}}
                    </a>
              </li>
          @else
               <li class="treeview">
                  <a href="#">
                    <i class="fa fa-dashboard"></i> <span>{{$nav->title}}</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-down pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu menu-open" style="display: block;">
                  @foreach($template['navs']['subs'] as $snav)
                  <?php
                    $props_sub = json_decode($snav->properties,true);

                            foreach ($props_sub as $key => $value) {
                              $prop_sub[$key] = '';
                              foreach ($props_sub[$key] as $keyx => $valuex) {
                                $prop_sub[$key] .= " $keyx=\"$valuex\"";
                              }
                            }
                  ?>
                    @if($nav->id==$snav->parent_id)
                      <li <?=array_has($prop_sub, 'li')?$prop_sub['li']:''?> >
                            <a href="{{Route::has($snav->route)? route($snav->route) : 'error'}}" <?=array_has($prop_sub, 'a')?$prop_sub['a']:''?> >
                                <i <?=array_has($prop_sub, 'i')?$prop_sub['i']:''?> ></i>
                                {{$snav->title}}
                            </a>
                      </li>
                    @endif
                  @endforeach
                  </ul>
              </li>
          @endif
        @endforeach
         <ul>

      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
@stop
