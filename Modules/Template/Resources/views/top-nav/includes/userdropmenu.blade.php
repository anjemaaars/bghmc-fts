<!-- User Account Menu -->

  <!-- ______________HOME BUTTON______________ -->
	@if( ( $template['page_title'] ) == 'Personnel') 
	<li class="dropdown user user-menu">
		<div style="text-align:right;" class="searchs">
			<form>
				<input type="search" name="search" placeholder="Search File To Track.." class="form-control input-defualt" id="inputdefault search"/>
			</form>
		</div>
	</li>
	<li class="dropdown user user-menu">
		<a href="{{URL::route('inventory.index')}}" class="btn btn-function btn-view-admin">
			<i class="glyphicon glyphicon-home"></i>Home
		</a>
	</li>
	<li class="dropdown user user-menu">
		<a href="{{URL::route('display.RECEIVED')}}" class="btn btn-function btn-view-admin">
			<i class="fa fa-inbox"></i>Inbox
		</a>
	</li>

	@elseif( ( $template['page_title'] ) == 'Administrator') 
	<li class="dropdown user user-menu">
		<div style="text-align:right;" class="searchs">
			<form>
				<input type="search" name="search" placeholder="Search File To Track.." class="form-control input-defualt" id="inputdefault search"/>
			</form>
		</div>
	</li>
	<li class="dropdown user user-menu">
		<a href="{{URL::route('admin.index')}}" class="btn btn-function btn-view-admin">
			<i class="glyphicon glyphicon-home"></i>Home
		</a>
	</li>
	<li class="dropdown user user-menu">
		<a href="{{URL::route('displayAdmin.RECEIVED')}}" class="btn btn-function btn-view-admin">
			<i class="fa fa-inbox"></i>Inbox
		</a>
	</li>
	@else
		<a href="/" class="btn btn-function btn-view-admin">
			<i class="glyphicon glyphicon-triangle"></i>&nbsp;&nbsp;&nbsp;&nbsp;Something Went Wrong!
		</a>
@endif

<li class="dropdown user user-menu">
  <!-- Menu Toggle Button -->
  
</li>