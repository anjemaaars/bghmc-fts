@extends('template::top-nav.default') 
@section('content')
    <div class="login-box">
        <center>
            <h3>We detected a default password.</h3>
            <h4>You are required to enter a new password which shall be used for your system sessions.</h4>
        </center>
        <div class="login-box-body">
            <form name="changePasswordform" id="changePasswordform">
                {{csrf_field()}}
                <div id="status"></div>
                <input type="hidden" name="empid" value="{{$empid}}">
                <div class="form-group has-feedback">
                    <input type="password" class="form-control" name="password" placeholder="New Password">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="password" class="form-control" name="repeat_password" placeholder="Re-type New Password">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="form-group">
                    <a onClick="$(this).changePassword();" class="btn btn-bts-login btn-function btn-flat">Save New Password and Sign Me In</a>
                </div>
            </form>
        </div>
    </div>
@stop 
@section('plugins-script')
    <script>
        $.fn.changePassword = function () {
            $.ajax({
                type: 'POST',
                url: "{{route('accnt.passchangepost')}}", //from routes
                data: $('#changePasswordform').serialize(),
                error: function () {
                    alert('error');
                },
                success: function (data) {
                    var errors = '';
                    if (data['status'] == 0) {
                        for (var key in data['errors']) {
                            errors += data['errors'][key] + '<br />';
                        }
                        $(' #changePasswordform  #status').html('<div class="alert alert-danger alert-dismissible"><h4><i class="icon fa fa-ban"></i> Alert!</h4>' +errors + '</div>').fadeIn();
                    } 
                    else {
                        $(' #changePasswordform  #status').html('<div class="alert alert-success alert-dismissible"><h4><i class="icon fa fa-check"></i>' +data['errors']['message'] + '</h4></div>').fadeIn().delay(1500).fadeOut(1000);
                        if(data['accnttype'] == 'administrator'){ window.location = "{{route('admin.index')}}"; }
                        else{ window.location = "{{route('inventory.index')}}"; }
                    }
                }
            });
        };
    </script>
    @stop