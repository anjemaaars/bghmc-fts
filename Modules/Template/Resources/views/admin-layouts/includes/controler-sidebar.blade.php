  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark ">

    <div class="tab-content" style="max-height: 100%;">
        <a>
          <h3 class="control-sidebar-heading text-center" style="color: #fff;">RECENT ACTIVITY</h3>
        </a>
        <ul class="control-sidebar-menu">
        @if(count($logs) != 0)
          @foreach($logs as $log)
          <li>
            <a href="javascript:void(0)">
              @if(strpos($log->type, 'new') !== false)
                <i class="menu-icon glyphicon glyphicon-plus" style="color:LightGreen;"></i>
              @elseif(strpos($log->type, 'update') !== false)
                <i class="menu-icon glyphicon glyphicon-pencil" style="color:yellow;"></i>
              @endif
              <div class="menu-info">
                <h4 class="control-sidebar-subheading">{{$log->log}}</h4>
                <p>by: #{{$log->logged_user}} ({{\Carbon\Carbon::parse($log->created_at)->format('M d Y h:m A')}})</p>
              </div>
            </a>
          </li>
          @endforeach
        @else
          <li>
            <center>No Activities for today</center>
          </li>
        @endif
        </ul>
    </div>
  </aside>
  <!-- /.control-sidebar -->

    <div class="control-sidebar-bg"></div>