  <header class="main-header">
    <!-- Logo -->
    <a href="{{url('administrator')}}" class="logo left-sidebar-header">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <!-- <span class="logo-mini"><b>A</b>LT</span> -->
      <!-- logo for regular state and mobile devices -->
      <span class="logo-mini">
          <span title="BGHMC-FTS"><b>BGHMC-FTS</b></span>
      </span>
      <span class="logo-lg">
        <img src="{{asset('img')}}/site-logo.jpg" style="width: 40%; margin: 10px;">
      </span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <!-- <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a> -->
      
      <ul class="nav navbar-nav">
          <li>
            <a style="cursor: default;">@yield('pagename')</a>
          </li>
        </ul>
      @include('template::admin-layouts.includes.custom-menu')
    </nav>
  </header>

  <!-- =============================================== -->