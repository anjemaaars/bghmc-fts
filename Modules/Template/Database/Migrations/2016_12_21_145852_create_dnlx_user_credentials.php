<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDnlxUserCredentials extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(config('app.projcode').'_emp_credentials', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->integer('emp_id');
            $table->string('password');
            $table->integer('group_id')->unsigned()->comment('refer to '.config('app.projcode').'_user_groups'); //account type
            $table->string('remember_token')->nullable();
            $table->boolean('isactive');
            $table->timestamps();
            $table->softDeletes();
            $table->boolean('is_approved');

            $table->index(['id', 'group_id', 'emp_id'],config('app.projcode').'_emp_credentials');
        });

        # Insert
        DB::table(config('app.projcode').'_emp_credentials')->insert(array(
            [
                'emp_id' => '2017',
                'password' => '$2y$10$oyqauLvd34lHcseLRz/6yusVnMba./Y48jIA1giIJhFwW3qGKDjkq', //hashed "root" password
                'group_id' => 1, //1 is administrator
                'created_at' => date('Y-m-d H:i:s'),
                'isactive' => 1,
                'is_approved' => 1,
            ],
            [
                'emp_id' => '2018',
                'password' => '$2y$10$oyqauLvd34lHcseLRz/6yusVnMba./Y48jIA1giIJhFwW3qGKDjkq',
                'group_id' => 2, //2 is personnel
                'created_at' => date('Y-m-d H:i:s'),
                'isactive' => 1,
                'is_approved' => 1,
            ]
        ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(config('app.projcode').'_emp_credentials');
    }
}
