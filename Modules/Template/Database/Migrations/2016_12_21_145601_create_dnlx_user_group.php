<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDnlxUserGroup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if (Schema::hasTable(config('app.projcode').'_user_groups')) {
            Schema::dropIfExists(config('app.projcode').'_user_groups');
        }
                Schema::create(config('app.projcode').'_user_groups', function (Blueprint $table) {
                    $table->engine = 'InnoDB ROW_FORMAT=DYNAMIC';

                    $table->increments('id');
                    $table->string('ugrp_name',50)->unique();
                    $table->string('ugrp_description');
                    $table->string('ugrp_homepage');
                    $table->timestamps();
                    // $table->index(['id', 'ugrp_homepage'],config('app.projcode').'_user_groups_key');
                });

                # Insert
                DB::table(config('app.projcode').'_user_groups')->insert(array(
                    [
                        'ugrp_name' => 'Administrator',
                        'ugrp_homepage' => 'admin.index',
                        'ugrp_description' => 'Privileged users'
                    ],
                    [
                        'ugrp_name' => 'Personnel',
                        'ugrp_homepage' => 'inventory.index',
                        'ugrp_description' => 'None Priveledged users '
                    ]
                ));



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(config('app.projcode').'_user_groups');
    }
}
