<?php

namespace Modules\Template\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class TemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('template::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('template::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('template::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('template::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    public function changep_view($empid){
        $this->data['empid'] = $empid;
        return view('template::change_default_pass',$this->setup());
    }
    public function update_password(Request $request){
        var_dump('entered update_password function in LoginController!');
        // $data = $request->all();
        
        // $PM = new PM; // access Personnel Model for the database tables
        
        // if($PM->validate($data, 'change_password')) // validate if all fields are correct
        // {
        //     // $PM->changePassword($request); //Proceeds to Personnel Model to process input
        //     $data['status'] = 1;
        //     $data['errors']['message'] = 'Password Changed';
        // }
        // else
        // {
        //     $data['status'] = 0;
        //     $data['errors'] = $PM->errors();
        // }
        // return $data;
    }
}
