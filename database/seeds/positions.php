<?php

use Illuminate\Database\Seeder;

class positions extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // create fake data to fill database
        $faker = Faker\Factory::create();
        $dt_created = Carbon\Carbon::now();

        $limit = 9;
        $positions = array(
            "hospital chief", 
            "department head",
            "administrative officer",
            "administrative assistant",
            "training officer",
            "medical officer",
            "medical specialist",
            "head nurse",
            "nurse"
        );

        for ($i = 0; $i < $limit; $i++) {
            DB::table('bghmc_positions')->insert([
                'pos_id' => '8000' . ($i+1),
                'pos_name' => $positions[$i],
                'created_at' => $dt_created->toDateTimeString(),
            ]);
        }
    }
}
