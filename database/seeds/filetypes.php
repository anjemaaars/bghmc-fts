<?php

use Illuminate\Database\Seeder;

class filetypes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // create fake data to fill database
        $faker = Faker\Factory::create();
        $dt_created = Carbon\Carbon::now();

        $limit = 4;
        $file_types = array("Communication letter", "Purchase order", "Medical Records", "Department Vouchers", "Billing Statement");

        for ($i = 0; $i < $limit; $i++) {
            DB::table('bghmc_filetypes')->insert([
                'filetype_id' => '7000' . ($i+1),
                'filetype_name' => $file_types[$i],
                'created_at' => $dt_created->toDateTimeString()
            ]);
        }
    }
}
